import hashlib
import psycopg2
import json

class CSql():
    def __init__(self):
        self.h = None

    def omConnect(self, p_cDB = None):
        llOk = True
        if p_cDB == 2:
            lcConnect = "host=localhost dbname=UCSMINS user=postgres password=postgres port=5432"
        elif p_cDB == 3:
            lcConnect = "host=localhost dbname=UCSMASBANC user=postgres password=postgres port=5432"
        else:
            lcConnect = "host=localhost dbname=UCSMERP user=postgres password=postgres port=5432"
        try:
           self.h = psycopg2.connect(lcConnect) 
        except psycopg2.DatabaseError:
           print ('Error Conectando')
           llOk = False
           self.pcError = '{"ERROR": "ERROR AL CONECTAR CON LA BASE DE DATOS"}'
        return llOk

    def omExecRS(self, p_cSql):
        lcCursor = self.h.cursor()
        try: 
          lcCursor.execute(p_cSql)
        except psycopg2.Error as e:
          print(e)
          self.pcError = '{"ERROR": "ERROR AL EJECUTAR CONSULTA"}'
          return False

        RS = lcCursor.fetchall()
        '''
        if len(RS) == 0:
           RS = None
        try:
           i = RS[0]
        except:
           RS = None
        '''
        return RS

    def omExec(self, p_cSql):
        llOk = True
        lcCursor = self.h.cursor()
        try:
           lcCursor.execute(p_cSql)
        except Exception as E:
           print (type(E) )    # the exception instance
           print (E.args)      # arguments stored in .args
           print (E)           # __str__ allows args to printed directly:
           print ('***')
           llOk = False
        return llOk

    def omDisconnect(self):
        self.h.close()

    def omCommit(self):
        self.h.commit()

