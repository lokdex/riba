<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('content-type: application/json; charset=utf-8');

$REQUEST = file_get_contents('php://input');
//define("DATA", json_decode($REQUEST, true));
$DATA = json_decode($REQUEST, true);

$pcQuery = $DATA['query'];

if ($pcQuery == 'Index')
    fxIndex();
else if ($pcQuery == 'Consultar')
    fxConsultar();
else if ($pcQuery == 'ConsultarEventos')
    fxConsultarEventos();
else if ($pcQuery == 'GrabarFolder')
    fxGrabarFolder();
else if ($pcQuery == 'InitFolders')
    fxInitFolders();
else if ($pcQuery == 'ConsultarFolders')
    fxConsultarFolders();
else if ($pcQuery == 'CargarParticipantes')
    fxCargarParticipantes();
else if ($pcQuery == 'BusquedaTitulo')
    fxBusquedaTitulo();
else if ($pcQuery == 'BuscarCursos')
    fxBusquedaCursos();
else if ($pcQuery == 'BuscarDocentes')
    fxBusquedaDocente();
else if ($pcQuery == 'GrabarParticipantes')
    //fxGrabarParticipantes();
    ;
else
    echo json_encode(['ERROR' => 'CONEXIÓN INCORRECTA']);

function fxIndex()
{
    echo "Llega aquí";
    /*
    global $DATA;
    $lo = new CEventos();
    $lo->paData = $DATA;
    $llOk = $lo->omIndex();
    if (!$llOk)
        echo json_encode($lo->paError);
    else
        echo json_encode($lo->paDatos);
    */
}

function fxInitFolders()
{
    global $DATA;
    $laData = ['ID' => 'FPG1610I', 'CUSUCOD' => '1221'];
    $lcParam = json_encode($laData);
    $lcCommand = "python CRiba.py '" . $lcParam . "'";
    $laArray = fxCargarResultado($lcCommand);
    if (isset($laArray['ERROR'])) {
        echo json_encode($laArray['ERROR']);
    }
    echo json_encode($laArray);
}

function fxConsultarFolders()
{
    $laData = ['ID' => 'FPG1610F', 'CUSUCOD' => '1221'];
    $lcParam = json_encode($laData);
    $laArray = fxExecute($lcParam);
    if (isset($laArray['ERROR'])) {
        echo json_encode($laArray['ERROR']);
    }
    echo json_encode($laArray);
}

function fxGrabarFolder()
{
    global $DATA; //revisar cómo llega
    $DATA = $DATA['paData'];
    $laData = [
        'ID' => 'FPG1610G', 'CIDFOLD' => $DATA['CIDFOLD'], 'CTIPO' => $DATA['CTIPO'],
        'CESTADO' => $DATA['CESTADO'], 'CPROYEC' => $DATA['CPROYEC'], 'CCODDOC' => $DATA['CCODDOC'],
        'CCODCUR' => $DATA['CCODCUR'], 'CTIPCUR' => $DATA['CTIPCUR'], 'CGRUSEC' => $DATA['CGRUSEC'],
        'CFASE' => $DATA['CFASE'], 'CSUBFAS' => $DATA['CSUBFAS'], 'DFECHA' => $DATA['DFECHA'],
        'MTITULO' => $DATA['MTITULO'], 'MLINK' => $DATA['MLINK'], 'CEVALUA' => $DATA['CEVALUA'],
        'CEVENTO' => $DATA['CEVENTO'], 'CUSUCOD' => '1221'
    ];
    $lcParam = json_encode($laData);
    $laArray = fxExecute($lcParam,1);
    if (isset($laArray['ERROR'])) {
        echo json_encode($laArray['ERROR']);
    }
    http_response_code(200);
    echo json_encode($laArray);
}

function fxCargarParticipantes()
{
    global $DATA;
    $laData = ['ID' => 'FPG1610A', 'CIDFOLD' => $DATA['CIDFOLD'], 'CUSUCOD' => '1221'];
    $lcParam = json_encode($laData);
    $lcCommand = "python CRiba.py '" . $lcParam . "'";
    $laArray = fxCargarResultado($lcCommand);
    if (isset($laArray['ERROR'])) {
        echo json_encode($laArray['ERROR']);
    }
    echo json_encode($laArray);
}

function fxBusquedaTitulo()
{
    global $DATA;
    $DATA = $DATA['paData'];
    $laData = ['ID' => 'FPG1016T', 'CBUSTIT' => $DATA['CBUSTIT']];
    $lcParam = json_encode($laData);
    $laArray = fxExecute($lcParam);
    if (isset($laArray['ERROR'])) {
        echo json_encode($laArray['ERROR']);
    }
    http_response_code(200);
    echo json_encode($laArray);
}

function fxBusquedaDocente(){
    $laData = ['ID' => 'FPG1016D'];
    $lcParam = json_encode($laData);
    $laArray = fxExecute($lcParam);
    if (isset($laArray['ERROR'])) {
        echo json_encode($laArray['ERROR']);
    }
    http_response_code(200);
    echo json_encode($laArray);
}

function fxBusquedaCursos(){
    $laData = ['ID' => 'FPG1016U'];
    $lcParam = json_encode($laData);
    $laArray = fxExecute($lcParam);
    if (isset($laArray['ERROR'])) {
        echo json_encode($laArray['ERROR']);
    }
    http_response_code(200);
    echo json_encode($laArray);
}

/*
function fxGrabarParticipantes(){
    global $DATA;
    $laData = ['ID' => 'FPG1610P', 'CIDFOLD' => $DATA['CIDFOLD'], 'CUSUCOD' => '1221',
        'ADATOS' => ['CNRODNI' => ]];
        "ADATOS": [{"CNRODNI": "29244573", "CTIPO": "D", "NNOTA": "20", "NPORPAR": "50"},
        {"CNRODNI": "29393323", "CTIPO": "D", "NNOTA": "20", "NPORPAR": "50"}]}'
}

function fxConsultarEventos()
{
    //$laData = ['ID' => 'FPG']
    $lcCommand = "python CRiba.py '" . $lcParam . "'";
    $laArray = fxCargarResultado($lcCommand);
    echo json_encode($laArray);
}
*/

function fxExecute($p_cParam, $debug = 0)
{
    $lcCommand = "python3 CRiba.py '" . $p_cParam . "'";
    $lcData = shell_exec($lcCommand);
    if ($debug == 1) {
        $lcDebug = ['Command' => $lcCommand, 'Resultado' => $lcData];
        http_response_code(404);
        return json_encode($lcDebug);
    }
    return json_decode($lcData, true);
}

function fxCargarResultado($p_cCommand)
{
    //echo '<br>'.$p_cCommand.'<br>';
    $lcData = shell_exec($p_cCommand);
    //echo '<br>'.$lcData.'<br>';
    $laData = json_decode($lcData, true);
    return $laData;
}

//python CRiba.py '{"ID":"FPG1610G","CIDFOLD":"*","CTIPO":"E","CESTADO":"A","CPROYEC":"2020-1","CCODDOC":"1221","CCODCUR":"1001002","CTIPCUR":"P","CGRUSEC":"01","CFASE":"1","CSUBFAS":"0","DFECHA":"2020-08-20","MTITULO":"C\u00f3mo descuartizar a Diego","MLINK":"www.google.com","CEVALUA":"S\/E","CEVENTO":"0000","CUSUCOD":"1221"}'
