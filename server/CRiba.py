#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, getopt, json
from CBase import *
from CSql import *
from datetime import date
import datetime

def fxValidarCadena(p_mCadena):
    for lcChar in p_mCadena:
        #print(lcChar, ord(lcChar))
        if (32 <= ord(lcChar) <= 90) or (97 <= ord(lcChar) <= 122) or lcChar in ['á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú', 'ñ', 'Ñ']:
           pass
        else:
           return False
        if ord(lcChar) in [34, 39]:
           return False
    return True

def fxValInteger(p_xValor):
    #print(str(type(p_xValor)))
    if 'int' in str(type(p_xValor)):
       return True
    elif 'str' in str(type(p_xValor)):
       pass
    else:
       return False
    #print(p_xValor.isnumeric())
    return p_xValor.isnumeric()

# ----------------------------------------------------------------------
# Clase que gestiona los folders para la acreditacion RIBA
# ----------------------------------------------------------------------
class CRiba(CBase):

    def __init__(self):
        self.paData = None
        self.loSql = CSql()

    def mxValParam(self):
        if not 'CUSUCOD' in self.paData or len(self.paData['CUSUCOD']) != 4:
           self.pcError = 'CODIGO DE USUARIO NO DEFINIDO O INVALIDO'
           return False
        return True

    def mxValUsuario(self, p_cCodRol):
        lcSql = "SELECT cEstado FROM S01TUSU WHERE cCodUsu = '%s'"%(self.paData['CUSUCOD'])
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or len(R1) == 0:
           self.pcError = 'CODIGO DE USUARIO NO EXISTE'
           return False
        elif R1[0][0] != 'A':
           self.pcError = 'CODIGO DE USUARIO NO ESTA ACTIVO'
           return False
        lcSql = "SELECT cEstado FROM S01PROL WHERE cCodUsu = '%s' AND cCodRol = '%s'"%(self.paData['CUSUCOD'], p_cCodRol)
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or len(R1) == 0:
           self.pcError = 'CODIGO DE USUARIO NO EXISTE EN TABLA DE ROLES'
           return False
        elif R1[0][0] != 'A':
           self.pcError = 'CODIGO DE USUARIO NO ESTA ACTIVO EN TABLA DE ROLES'
           return False
        return True

    # ---------------------------------------------------------------
    # Init para el registro y mantenimiento de folder
    # 2020-08-18 FPM Creacion
    # ---------------------------------------------------------------
    def omInitMtoFolder(self):
        llOk = self.mxValParam()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxInitMtoFolder()
        self.loSql.omDisconnect()
        return llOk

    def mxInitMtoFolder(self):
        # Select para los tipos
        laData = {'ATIPFOL': '', 'ATIPCUR': '', 'ATIPPAR': '', 'ATIPBIB': '', 'ACODCUR': '', 'ACODDOC': '', 'AEVENTO': ''}
        laTipos = [['ATIPFOL', '274'], ['ATIPCUR', '275'], ['ATIPPAR', '276'], ['ATIPBIB', '277']]
        for laFila in laTipos:
            i = 0
            laDatos = []
            lcSql = "SELECT SUBSTRING(cCodigo, 1, 1), cDescri FROM V_S01TTAB WHERE cCodTab = '%s'"%(laFila[1])
            R1 = self.loSql.omExecRS(lcSql)
            for r in R1:
                i += 1
                laTmp = {'CCODIGO': r[0], 'CDESCRI': r[1].strip()}
                laDatos.append(laTmp)
            if i == 0:
               self.pcError = 'TABLA DE TIPOS [%s] NO ESTA DEFINIDA'%(laFila[1])
               return False
            laData[laFila[0]] = laDatos
        
        # Select para los cursos
        lcSql = "SELECT ccodcur, cdescri, cplaest, cestado FROM a02mcur WHERE cuniaca = '41' ORDER BY ccodcur"
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or not R1[0][0]:
            self.pcError = 'ERROR AL RECUPERAR CURSOS'
            return False
        i = 0
        laDatos = []
        for r in R1:
            row = {'CCODCUR': r[0], 'CDESCRI': r[1], 'CPLAEST': r[2], 'CESTADO': r[3]}
            i += 1
            laDatos.append(row)
        laData['ACODCUR'] = laDatos
        
        # Select para los docentes
        lcSql = "SELECT cCodDoc, cNombre FROM V_A01MDOC ORDER BY cNombre" 
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or not R1[0][0]:
            self.pcError = 'ERROR AL RECUPERAR DOCENTES'
            return False
        i = 0
        laDatos = []
        for r in R1:
            row = {'CCODDOC': r[0], 'CNOMBRE': r[1]}
            i += 1
            laDatos.append(row)  
        laData['ACODDOC'] = laDatos

        # Select para eventos
        lcSql = "select cEvento, cDesTip FROM V_F02MEVE ORDER BY cEvento"
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or not R1[0][0]:
            self.pcError = 'ERROR AL RECUPERAR EVENTOS'
            return False
        i = 0
        laDatos = []
        for r in R1:
            row = {'CEVENTO': r[0], 'CDESTIP': r[1]}
            i += 1
            laDatos.append(row) 
        laData['AEVENTO'] = laDatos

        #Retorna todo junto
        self.paData = laData
        return True

    # ---------------------------------------------------------------
    # Cargar cabecera de folder
    # 2020-08-19 FPM Creacion
    # ---------------------------------------------------------------
    def omCargarFolder(self):
        llOk = self.mxValParamCargarFolder()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxCargarFolder()
        if llOk:
           self.loSql.omCommit()
        self.loSql.omDisconnect()
        return llOk

    def mxValParamCargarFolder(self):
        loDate = CDate()
        if not self.mxValParam():
           return False
        elif not 'CIDFOLD' in self.paData or len(self.paData['CIDFOLD']) != 8:
           self.pcError = 'ID DE FOLDER NO DEFINIDO O INVALIDO'
           return False
        return True

    def mxCargarFolder(self):
        lcSql = """SELECT cTipo, cProyec, cCodDoc, cCodCur, cTipCur, cGruSec, cFase, cSubFas, TO_CHAR(dFecha, 'YYYY-MM-DD'),
                   mTitulo, mLink, cEvalua, cEvento FROM F02MFOL WHERE cIdFold = '%s'"""%(self.paData['CIDFOLD'])
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or not R1[0][0]:
           self.pcError = 'ID DE FOLDER [%s] NO EXISTE'%(self.paData['CIDFOLD'])
           return False
        r = R1[0]
        self.paData = {'CTIPO':   r[0],  'CPROYEC': r[1],  'CCODDOC': r[2], 'CCODCUR': r[3], 'CTIPCUR': r[4], \
                       'CGRUSEC': r[5],  'CFASE':   r[6],  'CSUBFAS': r[7], 'DFECHA':  r[8], 'MTITULO': r[9], \
                       'MLINK':   r[10], 'CEVALUA': r[11], 'CEVENTO': r[12]}
        return True

    # ---------------------------------------------------------------
    # Grabar cabecera de folder
    # 2020-08-17 FPM Creacion
    # ---------------------------------------------------------------
    def omGrabarFolder(self):
        llOk = self.mxValParamGrabarFolder()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxGrabarFolder()
        if llOk:
           self.loSql.omCommit()
        self.loSql.omDisconnect()
        return llOk

    def mxValParamGrabarFolder(self):
        loDate = CDate()
        if not self.mxValParam():
           return False
        elif not 'CIDFOLD' in self.paData or not len(self.paData['CIDFOLD']) in [1, 8]:
           self.pcError = 'ID DE FOLDER NO DEFINIDO O INVALIDO'
           return False
        elif not 'DFECHA' in self.paData or not loDate.mxValDate(self.paData['DFECHA']):
           self.pcError = 'FECHA NO DEFINIDA O INVALIDA'
           return False
        elif not 'CPROYEC' in self.paData or len(self.paData['CPROYEC']) > 8:
           self.pcError = 'PROYECTO NO DEFINIDO O INVALIDO'
           return False
        elif not 'CCODDOC' in self.paData or len(self.paData['CCODDOC']) != 4:
           self.pcError = 'DOCENTE NO DEFINIDO O INVALIDO'
           return False
        elif not 'CCODCUR' in self.paData or len(self.paData['CCODCUR']) != 7:
           self.pcError = 'CURSO NO DEFINIDO O INVALIDO'
           return False
        elif not 'CTIPO' in self.paData or len(self.paData['CTIPO']) != 1:
           self.pcError = 'TIPO DE FOLDER NO DEFINIDO O INVALIDO'
           return False
        elif not 'CTIPCUR' in self.paData or len(self.paData['CTIPCUR']) != 1:
           self.pcError = 'TIPO DE CURSO NO DEFINIDO O INVALIDO'
           return False
        elif not 'CGRUSEC' in self.paData or not len(self.paData['CGRUSEC']) in [1, 2]:
           self.pcError = 'GRUPO/SECCION DE CURSO NO DEFINIDO O INVALIDO'
           return False
        elif not 'CFASE' in self.paData or len(self.paData['CFASE']) != 1:
           self.pcError = 'FASE DE CURSO NO DEFINIDO O INVALIDO'
           return False
        elif not 'CSUBFAS' in self.paData or len(self.paData['CSUBFAS']) != 1:
           self.pcError = 'SUB-FASE DE CURSO NO DEFINIDO O INVALIDO'
           return False
        elif not 'CEVENTO' in self.paData or len(self.paData['CEVENTO']) != 4:
           self.pcError = 'EVENTO NO DEFINIDO O INVALIDO'
           return False
        elif not 'MTITULO' in self.paData or len(self.paData['MTITULO']) > 250:
           self.pcError = 'TITULO NO DEFINIDO O INVALIDO'
           return False
        elif not fxValidarCadena(self.paData['MTITULO']):
           self.pcError = 'TITULO TIENE CARACTERES INVALIDOS'
           return False
        elif not 'MLINK' in self.paData or len(self.paData['MLINK']) > 250:
           self.pcError = 'ENLACE WEB NO DEFINIDO O INVALIDO'
           return False
        elif not fxValidarCadena(self.paData['MLINK']):
           self.pcError = 'ENLACE WEB TIENE CARACTERES INVALIDOS'
           return False
        elif not 'CEVALUA' in self.paData or len(self.paData['CEVALUA']) > 2:
           self.pcError = 'EVALUACION NO DEFINIDA O INVALIDA'
           return False
        return True

    def mxGrabarFolder(self):
        if self.paData['CIDFOLD'] == '*':
           lcIdFold = '00000000'
           lcSql = 'SELECT MAX(cIdFold) FROM F02MFOL'
           R1 = self.loSql.omExecRS(lcSql)
           if R1[0][0] != None:
              lcIdFold = R1[0][0]
           lcIdFold = '0000000' + str(int(lcIdFold) + 1)
           lcIdFold = lcIdFold[-8:]
           lcSql = """INSERT INTO F02MFOL (cIdFold, cTipo, cProyec, cCodDoc, cCodCur, cTipCur, cGruSec, cFase,
                      cSubFas, dFecha, mTitulo, mLink, cEvalua, cEvento, cUsuCod) VALUES ('%s', '%s', '%s', 
                      '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')""" % \
                      (lcIdFold, self.paData['CTIPO'], self.paData['CPROYEC'], self.paData['CCODDOC'], \
                      self.paData['CCODCUR'], self.paData['CTIPCUR'], self.paData['CGRUSEC'], \
                      self.paData['CFASE'], self.paData['CSUBFAS'], self.paData['DFECHA'], \
                      self.paData['MTITULO'], self.paData['MLINK'], self.paData['CEVALUA'], \
                      self.paData['CEVENTO'], self.paData['CUSUCOD'])
        else:
           lcSql = "SELECT cIdFold FROM F02MFOL WHERE cIdFold = '%s'"%(self.paData['CIDFOLD'])
           R1 = self.loSql.omExecRS(lcSql)
           if not R1 or not R1[0][0]:
              self.pcError = 'ID DE FOLDER [%s] NO EXISTE'%(self.paData['CIDFOLD'])
              return False
           lcSql = """UPDATE F02MFOL SET cTipo = '%s', cProyec = '%s', cCodDoc = '%s', cCodCur = '%s', 
                      cTipCur = '%s', cGruSec = '%s', cFase = '%s', cSubFas = '%s', dFecha = '%s', 
                      mTitulo = '%s', mLink = '%s', cEvalua = '%s', cEvento = '%s', cUsuCod = '%s', 
                      tModifi = NOW() WHERE cIdFold = '%s'"""%(self.paData['CTIPO'], self.paData['CPROYEC'],\
                      self.paData['CCODDOC'], self.paData['CCODCUR'], self.paData['CTIPCUR'], self.paData['CGRUSEC'], \
                      self.paData['CFASE'], self.paData['CSUBFAS'], self.paData['DFECHA'], self.paData['MTITULO'], \
                      self.paData['MLINK'], self.paData['CEVALUA'], self.paData['CEVENTO'], self.paData['CUSUCOD'], \
                      self.paData['CIDFOLD'])
        llOk = self.loSql.omExec(lcSql)
        if not llOk:
           self.pcError = 'NO SE PUDO GRABAR CONTENIDO DE FOLDER'
        return llOk

    # ---------------------------------------------------------------
    # Cargar participantes de folder
    # 2020-08-19 FPM Creacion
    # ---------------------------------------------------------------
    def omCargarParticipantes(self):
        llOk = self.mxValParamCargarFolder()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxCargarParticipantes()
        self.loSql.omDisconnect()
        return llOk

    def mxCargarParticipantes(self):
        laData = {'DATA': '', 'DATOS': ''}
        # Datos de la cabecera
        lcSql = """SELECT cIdFold, cDesTip, cProyec, cNombre, cCodCur, cDesCur, cEstado FROM V_F02MFOL 
                   WHERE cIdFold = '%s'"""%(self.paData['CIDFOLD'])
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or len(R1) == 0:
           self.pcError = 'ID DE FOLDER NO EXISTE'
           return False
        elif R1[0][6] != 'A':
           self.pcError = 'FOLDER NO ESTA ACTIVO'
           return False
        r = R1[0]
        laData['DATA'] = {'CIDFOLD': r[0], 'CDESTIP': r[1], 'CPROYEC': r[2], 'CNOMBRE': r[3].replace('/', ' '), \
                          'CCODCUR': r[4] + ' - ' + r[5]}
        # Participantes
        laDatos = []
        lcSql = """SELECT cNroDni, cNombre, cTipo, cDesFol, nNota, nPorPar FROM V_F02DPAR
                   WHERE cIdFold = '%s' ORDER BY cNombre"""%(self.paData['CIDFOLD'])
        R1 = self.loSql.omExecRS(lcSql)
        for r in R1:
            laTmp = {'CNRODNI': r[0], 'CNOMBRE': r[1].replace('/', ' '), 'CTIPO': r[2], 'CDESFOL': r[3],
                     'NNOTA': int(r[4]), 'NPORPAR': int(r[5])}
            laDatos.append(laTmp)
        laData['DATOS'] = laDatos
        self.paData = laData
        return True

    # ---------------------------------------------------------------
    # Grabar participantes de cabecera
    # 2020-08-17 FPM Creacion
    # ---------------------------------------------------------------
    def omGrabarParticipantes(self):
        llOk = self.mxValParamGrabarParticipantes()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxGrabarParticipantes()
        if llOk:
           self.loSql.omCommit()
        self.loSql.omDisconnect()
        return llOk

    def mxValParamGrabarParticipantes(self):
        loDate = CDate()
        if not self.mxValParam():
           return False
        elif not 'CIDFOLD' in self.paData or len(self.paData['CIDFOLD']) != 8:
           self.pcError = 'ID DE FOLDER NO DEFINIDO O INVALIDO'
           return False
        elif not 'ADATOS' in self.paData:
           self.pcError = 'DATOS DE PARTICIPANTES NO DEFINIDO'
           return False
        self.laDatos = self.paData['ADATOS']
        i = 0
        for laTmp in self.laDatos:
            if not 'CNRODNI' in laTmp or len(laTmp['CNRODNI']) != 8:
               self.pcError = 'DNI DE PARTICIPANTE NO DEFINIDO'
               return False
            elif not 'CTIPO' in laTmp or len(laTmp['CTIPO']) != 1:
               self.pcError = 'TIPO DE PARTICIPANTE NO DEFINIDO'
               return False
            elif not 'NNOTA' in laTmp or not fxValInteger(laTmp['NNOTA']):
               #print(laTmp)
               self.pcError = 'NOTA DE PARTICIPANTE NO DEFINIDO O INVALIDO'
               return False
            elif not 'NPORPAR' in laTmp or not fxValInteger(laTmp['NPORPAR']):
               self.pcError = 'PORCENTAJE DE PARTICIPANTE NO DEFINIDO O INVALIDO'
               return False
            self.laDatos[i]['NNOTA'] = int(self.laDatos[i]['NNOTA'])
            self.laDatos[i]['NPORPAR'] = int(self.laDatos[i]['NPORPAR'])
            i += 1
        return True

    def mxGrabarParticipantes(self):
        lcSql = "SELECT cEstado FROM F02MFOL WHERE cIdFold = '%s'"%(self.paData['CIDFOLD'])
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or not R1[0][0]:
           self.pcError = 'ID DE FOLDER [%s] NO EXISTE'%(self.paData['CIDFOLD'])
           return False
        elif R1[0][0] != 'A':
           self.pcError = 'ID DE FOLDER [%s] NO ESTA ACTIVO'%(self.paData['CIDFOLD'])
           return False
        lcSql = "DELETE FROM F02DPAR WHERE cIdFold = '%s'"%(self.paData['CIDFOLD'])
        llOk = self.loSql.omExec(lcSql)
        if not llOk:
           self.pcError = 'ERROR AL ELIMINAR ANTERIORES PARTICIPANTES DE FOLDER [%s]'%(self.paData['CIDFOLD'])
           return False
        for laTmp in self.laDatos:
            lcSql = """INSERT INTO F02DPAR (cIdFold, cNroDni, cTipo, nNota, nPorPar, cUsuCod) VALUES
                       ('%s', '%s', '%s', %s, %s, '%s')"""%(self.paData['CIDFOLD'], laTmp['CNRODNI'], \
                       laTmp['CTIPO'], laTmp['NNOTA'], laTmp['NPORPAR'], self.paData['CUSUCOD'])
            llOk = self.loSql.omExec(lcSql)
            if not llOk:
               self.pcError = 'ERROR AL INSERTAR PARTICIPANTES DE FOLDER [%s]'%(self.paData['CIDFOLD'])
               return False
        return llOk

    # ---------------------------------------------------------------
    # Cargar bibliografia de folder
    # 2020-08-25 FPM Creacion
    # ---------------------------------------------------------------
    def omCargarBibliografia(self):
        llOk = self.mxValParamCargarFolder()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxCargarBibliografia()
        self.loSql.omDisconnect()
        return llOk

    def mxCargarBibliografia(self):
        laData = {'DATA': '', 'DATOS': ''}
        # Datos de la cabecera
        lcSql = """SELECT cIdFold, cDesTip, cProyec, cNombre, cCodCur, cDesCur, cEstado FROM V_F02MFOL 
                   WHERE cIdFold = '%s'"""%(self.paData['CIDFOLD'])
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or len(R1) == 0:
           self.pcError = 'ID DE FOLDER NO EXISTE'
           return False
        elif R1[0][6] != 'A':
           self.pcError = 'FOLDER NO ESTA ACTIVO'
           return False
        r = R1[0]
        laData['DATA'] = {'CIDFOLD': r[0], 'CDESTIP': r[1], 'CPROYEC': r[2], 'CNOMBRE': r[3].replace('/', ' '), \
                          'CCODCUR': r[4] + ' - ' + r[5]}
        # Bibliografia
        laDatos = []
        lcSql = """SELECT cTipo, cDesTip, cIdLibr, cIdIsbn, mDatos FROM V_F02DBIB
                   WHERE cIdFold = '%s' ORDER BY cNombre"""%(self.paData['CIDFOLD'])
        R1 = self.loSql.omExecRS(lcSql)
        for r in R1:
            laTmp = {'CTIPO': r[0], 'CDESTIP': r[1], 'CIDLIBR': r[2], 'CIDISBN': r[3], 'MDATOS': r[4]}
            laDatos.append(laTmp)
        laData['DATOS'] = laDatos
        self.paData = laData
        return True

    # ---------------------------------------------------------------
    # Grabar bibliografia de folder
    # 2020-08-24 FPM Creacion
    # ---------------------------------------------------------------
    def omGrabarBibliografia(self):
        llOk = self.mxValParamGrabarBibliografia()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxGrabarBibliografia()
        if llOk:
           self.loSql.omCommit()
        self.loSql.omDisconnect()
        return llOk

    def mxValParamGrabarBibliografia(self):
        loDate = CDate()
        if not self.mxValParam():
           return False
        elif not 'CIDFOLD' in self.paData or len(self.paData['CIDFOLD']) != 8:
           self.pcError = 'ID DE FOLDER NO DEFINIDO O INVALIDO'
           return False
        elif not 'ADATOS' in self.paData:
           self.pcError = 'BIBLIOGRAFIA NO DEFINIDA'
           return False
        self.laDatos = self.paData['ADATOS']
        i = 0
        for laTmp in self.laDatos:
            i += 1
            if not 'CTIPO' in laTmp or len(laTmp['CTIPO']) != 1:
               self.pcError = 'TIPO DE BIBLIOGRAFIA NO DEFINIDO O INVALIDO'
               return False
            elif not 'CIDLIBR' in laTmp or len(laTmp['CIDLIBR']) != 12:
               self.pcError = 'ID DE LIBRO UCSM NO DEFINIDO O INVALIDO'
               return False
            elif not 'CIDISBN' in laTmp or len(laTmp['CIDISBN']) != 10:
               self.pcError = 'ISBN NO DEFINIDO O INVALIDO'
               return False
        if i == 0:
           self.pcError = 'NO HAY BIBLIOGRAFIA DEFINIDA'
           return False
        return True

    def mxGrabarBibliografia(self):
        lcSql = "SELECT cEstado FROM F02MFOL WHERE cIdFold = '%s'"%(self.paData['CIDFOLD'])
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or not R1[0][0]:
           self.pcError = 'ID DE FOLDER [%s] NO EXISTE'%(self.paData['CIDFOLD'])
           return False
        elif R1[0][0] != 'A':
           self.pcError = 'ID DE FOLDER [%s] NO ESTA ACTIVO'%(self.paData['CIDFOLD'])
           return False
        lcSql = "DELETE FROM F02DBIB WHERE cIdFold = '%s'"%(self.paData['CIDFOLD'])
        llOk = self.loSql.omExec(lcSql)
        if not llOk:
           self.pcError = 'ERROR AL ELIMINAR ANTERIOR BIBLIOGRAFIA DE FOLDER [%s]'%(self.paData['CIDFOLD'])
           return False
        for laTmp in self.laDatos:
            lcSql = """INSERT INTO F02DBIB (cIdFold, cTipo, cIdLibr, cIdIsbn, mDatos, cUsuCod) VALUES
                       ('%s', '%s', '%s', %s, '', '%s')"""%(self.paData['CIDFOLD'], laTmp['CTIPO'], \
                       laTmp['CIDLIBR'], laTmp['CIDISBN'], self.paData['CUSUCOD'])
            llOk = self.loSql.omExec(lcSql)
            if not llOk:
               self.pcError = 'ERROR AL INSERTAR BIBLIOGRAFIA DE FOLDER [%s]'%(self.paData['CIDFOLD'])
               return False
        return llOk

    # ---------------------------------------------------------------
    # Consultar todos los folders registrados
    # 2020-08-17 FPM Creacion
    # ---------------------------------------------------------------
    def omConsultarFolders(self):
        llOk = self.mxValParam()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxConsultarFolders()
        self.loSql.omDisconnect()
        return llOk

    def mxConsultarFolders(self):
        lcSql = """SELECT cIdFold, cTipo, cDesTip, cEstado, cProyec, cCodDoc, cNombre, cCodCur, cDesCur, cTipCur,
                   cGruSec, cFase, cSubFas, TO_CHAR(dFecha,'yyyy-mm-dd'), mTitulo, mLink, cEvalua, cEvento, cDesEve
                   FROM V_F02MFOL ORDER BY cIdFold"""
        R1 = self.loSql.omExecRS(lcSql)
        if not R1 or not R1[0][0]:
           self.pcError = 'ERROR AL RECUPERAR FOLDERS'
           return False
        i = 0
        laDatos = []
        for r in R1:
            row = {'CIDFOLD': r[0],  'CTIPO':   r[1],  'CDESTIP': r[2],  'CESTADO': r[3],  'CPROYEC': r[4], \
                   'CCODDOC': r[5],  'CNOMBRE': r[6],  'CCODCUR': r[7],  'CDESCUR': r[8],  'CTIPCUR': r[9], \
                   'CGRUSEC': r[10], 'CFASE':   r[11], 'CSUBFAS': r[12], 'DFECHA':  r[13], 'MTITULO': r[14], \
                   'MLINK':   r[15], 'CEVALUA': r[16], 'CEVENTO': r[17], 'CDESEVE': r[18]}
            i += 1
            laDatos.append(row)
        self.paDatos = laDatos
        return True

    # ---------------------------------------------------------------
    # Búsqueda por título de folders
    # 2020-08-17 FPM Creacion
    # ---------------------------------------------------------------
    def omBuscarTitulo(self):
        llOk = self.mxValParamBuscarTitulo()
        if not llOk:
           return False
        llOk = self.loSql.omConnect()
        if not llOk:
           self.pcError = self.loSql.pcError
           return False
        llOk = self.mxValUsuario('005')
        if not llOk:
           self.loSql.omDisconnect()
           return False
        llOk = self.mxBuscarTitulo()
        self.loSql.omDisconnect()
        return llOk

    def mxValParamBuscarTitulo(self):
        if not self.mxValParam():
           return False
        elif not 'CBUSTIT' in self.paData or fxValidarCadena(self.paData['CBUSTIT']) or\
           (5 <= len(self.paData['CBUSTIT']) <= 20):
           self.pcError = 'TITULO NO NO DEFINIDO O INVALIDO'
           return False
        return True

    def mxBuscarTitulo(self):
        lmTitulo = self.paData['CBUSTIT'].replace(' ', '%') + '%'
        lcSql = """SELECT cIdFold, cTipo, cDesTip, cEstado, cProyec, cCodDoc, cNombre, cCodCur, cDesCur, 
                   cTipCur, cGruSec, cFase, cSubFas, TO_CHAR(dFecha,'yyyy-mm-dd'), mTitulo, mLink, cEvalua,
                   cEvento, cDesEve FROM V_F02MFOL WHERE mTitulo LIKE '%s'"""%(lmTitulo)
        R1 = self.loSql.omExecRS(lcSql)
        laDatos = []
        for r in R1:
            row = {'CIDFOLD': r[0],  'CTIPO':   r[1],  'CDESTIP': r[2],  'CESTADO': r[3],  'CPROYEC': r[4], \
                   'CCODDOC': r[5],  'CNOMBRE': r[6],  'CCODCUR': r[7],  'CDESCUR': r[8],  'CTIPCUR': r[9], \
                   'CGRUSEC': r[10], 'CFASE':   r[11], 'CSUBFAS': r[12], 'DFECHA':  r[13], 'MTITULO': r[14], \
                   'MLINK':   r[15], 'CEVALUA': r[16], 'CEVENTO': r[17], 'CDESEVE': r[18]}
            laDatos.append(row)
        self.paDatos = laDatos
        return True


    # Devuelve error
    # Códigos             Acción
    # FPG1610-I           Init
    # FPG1610-C           Cargar Folder
    # FPG1610-G           Grabar Folder
    # FPG1610-P           Grabar Participantes
    # FPG1610-A           Cargar Participantes
    # FPG1610-B           Grabar Bibliografía
    # FPG1610-E           Cargar Bibliografía
    # FPG1610-F           Consultar folder
    # FPG1610-T           Búsqueda por título
    # FPG1016-D           Buscar de docentes
    # FPG1016-U           Buscar de cursos
