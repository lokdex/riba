#!/usr/bin/env python
# encoding=utf8
import json
import sys
import os
from flask import Flask, request, jsonify, send_file
from werkzeug.utils import secure_filename
from flask_cors import CORS

from datetime import datetime
from CRiba import *

# reload(sys)
# sys.setdefaultencoding('utf8')

app = Flask(__name__)
UPLOAD_FOLDER = './static'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg'}
app.config['MAX_CONTENT_PATH'] = 16*1024*1024
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)


# CORS(app)
# ------------------------------------------------
# WS Controlador de errores
# 2019-01-16 JLF Creacion
# ------------------------------------------------
@app.errorhandler(404)
def page_not_found(e):
    print(e)
    return '{"ERROR":"SOLICITUD INVALIDA"}'


@app.route('/FPG1610' ,methods=['POST'])
# Ruta de entrada para toda la aplicación
def f_FPG1610():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    if (laData['ID'] == 'IF'):
        llOk = lo.omInitMtoFolder()
    elif (laData['ID'] == 'FG'):
        llOk = lo.omGrabarFolder()
    elif (laData['ID'] == 'FC'):
        llOk = lo.omConsultarFolders()
    elif (laData['ID'] == 'PG'):
        llOk = lo.omGrabarParticipantes()
    elif (laData['ID'] == 'PC'):
        llOk = lo.omCargarParticipantes()
    elif (laData['ID'] == 'BG'):
        llOk = lo.omGrabarBibliografia()
    elif (laData['ID'] == 'BC'):
        llOk = lo.omCargarBibliografia()
    else:
        return {'ERROR': 'Código de operación no definido'}, 400
    if not llOk:
        return {'ERROR': lo.pcError}, 400
    return json.dumps(lo.paData) if lo.paData else json.dumps(lo.paDatos), 200


@app.route('/FPG1610I', methods=['POST'])
#
def f_FPG1610I():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omInitMtoFolder()
    if not llOk:
        return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paData)


@app.route('/FPG1016U', methods=['POST'])
#
def f_FPG1016U():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omBuscarCursos()
    if not llOk:
        return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paDatos)


@app.route('/FPG1016D', methods=['POST'])
#
def f_FPG1016D():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omBuscarDocentes()
    if not llOk:
        return {'ERROR': lo.pcError}
        # return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paDatos)


@app.route('/FPG1610G', methods=['POST'])
#
def f_FPG1610G():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omGrabarFolder()
    if not llOk:
        return {'ERROR': lo.pcError}, 400
        # return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paData), 200


@app.route('/FPG1610F', methods=['POST'])
#
def f_FPG1610F():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omConsultarFolders()
    if not llOk:
        return {'ERROR': lo.pcError}, 400
        # return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paDatos), 200


@app.route('/FPG1610P', methods=['POST'])
#
def f_FPG1610P():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omGrabarParticipantes()
    if not llOk:
        print(lo.pcError)
        return {'ERROR': lo.pcError}, 400
        # return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paData), 200


@app.route('/FPG1610A', methods=['POST'])
#
def f_FPG1610A():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omCargarParticipantes()
    if not llOk:
        print(lo.pcError)
        return {'ERROR': lo.pcError}, 400
        # return '{"ERROR": "%s"}' % (lo.pcError)
    print(lo.paData)
    return json.dumps(lo.paData), 200


@app.route('/FPG1610B', methods=['POST'])
#
def f_FPG1610B():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omGrabarBibliografia()
    if not llOk:
        return {'ERROR': lo.pcError}, 400
        # return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paDatos), 200


@app.route('/FPG1610E', methods=['POST'])
#
def f_FPG1610E():
    lcData = request.get_json(force=True)
    laData = json.loads(json.dumps(lcData))
    print(laData)
    lo = CRiba()
    lo.paData = laData
    llOk = lo.omCargarBibliografia()
    if not llOk:
        return {'ERROR': lo.pcError}, 400
        # return '{"ERROR": "%s"}' % (lo.pcError)
    return json.dumps(lo.paData), 200


#@app.route('/FPG1610S', methods=['POST'])
# Subida de documentos
def f_FPG1610UP():
    if request.method == 'POST':
        if 'myFile' not in request.files:
            return {'ERROR': 'NO SE CARGÓ UN ARCHIVO'}, 400
        file = request.files['myFile']
        if file.filename == '':
            return {'ERROR': 'NO SE CARGÓ UN ARCHIVO'}, 400
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return {'ERROR': 'ARCHIVO CARGADO EXITOSAMENTE'}, 200
            

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/documento', methods=['POST'])
#
def f_documento():
    return "/prueba.pdf"

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['myFile']
        f.save(f.filename)
        return 'file uploaded successfully'


if __name__ == '__main__':
    # app.run(host='localhost', debug=True, port=8080)
    app.run(host='0.0.0.0', debug=True, port=8082, threaded=True)
    # app.run(host='192.168.0.3', debug=True, port=8080)
