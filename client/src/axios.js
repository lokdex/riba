import axios from "axios";

const fetch = axios.create({
   baseURL: "http://0.0.0.0:8082/",
});

export default fetch;
