import { Redirect } from "@reach/router";

const getAccessToken = () => sessionStorage.getItem("CCODUSU");
const isAuthenticated = () => !!getAccessToken();

export const PrivateRoute = ({ component: Component, path, ...props }) =>
   isAuthenticated() ? (
      <Component {...props} />
   ) : (
      <Redirect from={path} to="/login" />
   );

export const PublicRoute = ({ component: Component, path, ...props }) =>
   isAuthenticated() ? (
      <Component {...props} />
   ) : (
      <Redirect from={path} to="/" />
   );
