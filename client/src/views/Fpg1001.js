import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";

const Fpg1001 = () => {
   let [state, setState] = useState({});
   const { register, errors, handleSubmit } = useForm();

   const onSubmit = (formData) => {
      setState(formData);
   };

   return (
      <div className="container">
         <div className="card">
            <div class="card-header" style={styles.header}>
               <h2>Login</h2>
            </div>

            <form
               onSubmit={handleSubmit(onSubmit)}
               className="container-fluid pt-3"
            >
               <div className="form-group row">
                  <label htmlFor="CCODUSU" className="col-sm-2 col-form-label">
                     Codigo
                  </label>
                  <div className="col-sm-10">
                     <input
                        type="text"
                        name="CCODUSU"
                        id="CCODUSU"
                        ref={register(val.CCODUSU)}
                        className="form-control"
                     />
                     {<p style={styles.error}>{errors?.CCODUSU?.message}</p>}
                  </div>
               </div>

               <div className="form-group row">
                  <label htmlFor="CCLAVE" className="col-sm-2 col-form-label">
                     Contraseña
                  </label>
                  <div className="col-sm-10">
                     <input
                        type="password"
                        name="CCLAVE"
                        id="CCLAVE"
                        ref={register(val.CCLAVE)}
                        className="form-control"
                     />
                     {<p style={styles.error}>{errors?.CCLAVE?.message}</p>}
                  </div>
               </div>

               <div className="col-sm-10 offset-sm-2">
                  <input
                     style={styles.btn}
                     type="submit"
                     value="Iniciar Sesion"
                     className="btn btn-success"
                  />
               </div>

               <pre>{JSON.stringify(state, undefined, 4)}</pre>
            </form>
         </div>
      </div>
   );
};

const styles = {
   error: {
      color: "red",
      margin: 0,
   },
   header: {
      backgroundColor: "#f8AA1A",
      textAlign: "center",
      fontWeight: "bold",
   },
   btn: {
      backgroundColor: "#245433",
   },
};

const val = {
   CCODUSU: {
      required: {
         value: true,
         message: "Este campo es obligatorio",
      },
   },
   CCLAVE: {
      required: {
         value: true,
         message: "Este campo es obligatorio",
      },
   },
};

export default Fpg1001;
