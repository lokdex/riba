/*

-bib
descr folder
descr tipo
*/
import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "@reach/router";
import { useForm } from "react-hook-form";

import axios from "../../axios";

const Bibliografia = () => {
   const navigate = useNavigate();
   /*data fetch */
   const [selected, setSelected] = useState(null);
   const [bibliografia, setBibliografia] = useState([]);
   const [loading, setLoading] = useState(false);
   const [error, setError] = useState();

   const cargarBibliografia = async () => {
      try {
         setLoading(true);
         let response = await axios.post("/FPG1610E", { CUSUCOD: "1221" });
         console.log(response.data);
         let datos = response?.data?.map((dato) => ({
            ...dato,
            key: dato.NSERIAL,
         }));
         setBibliografia(datos);
      } catch (error) {
         if (error.response) {
            setError(error.response.data);
         } else if (error.request) {
            setError("Error de red");
         } else {
            setError("Error desconocido");
         }
         console.log(error);
      } finally {
         setLoading(false);
      }
   };

   useEffect(() => {
      cargarBibliografia();
   }, []);

   useEffect(() => {
      error &&
         alert.error({
            title: "Error",
            content: error,
         });
   }, [error]);

   const editar = () => {
      let seleccionado;
      bibliografia.forEach((dato) => {
         if (dato.NSERIAL == selected) seleccionado = dato;
      });
      navigate("Fpg1120/Fpg1124", { state: seleccionado });
   };

   const columns = [
      {
         title: "ID",
         dataIndex: "NSERIAL",
      },
      {
         title: "DESCR. FOLDER",
         dataIndex: "CDESFOL",
      },
      {
         title: "TIPO",
         dataIndex: "CDESTIP",
      },
   ];

   return (
      <div className="container-fluid">
         <br />
         <div
            className="card"
            style={{
               margin: "100px",
            }}
         >
            <div className="card-body">
               <div
                  className="card-title"
                  style={{ fontSize: "20px", fontWeight: "500" }}
               >
                  Bibliografía registrada -{" "}
                  {"Agrega aquí el cdesfol que viene del query"}
               </div>
               <table className="table table-striped">
                  <thead
                     style={{
                        color: "white",
                        backgroundColor: "rgb(71,145,77)",
                     }}
                  >
                     <tr>
                        <th scope="col">TIPO</th>
                        <th scope="col">DESCRIPCIÓN</th>
                        <th scope="col">ID LIB.</th>
                        <th scope="col">ISBN</th>
                        <th scope="col">DATOS</th>
                        <th scope="col"></th>
                     </tr>
                  </thead>
                  <tbody>
                     {bibliografia.length ? (
                        bibliografia.map((item, idx) => (
                           <tr>
                              <td>{item.CIDFOLD}</td>
                              <td>{item.MTITULO}</td>
                              <td>{item.CNOMBRE}</td>
                              <td>{item.CDESCUR}</td>
                              <td>{item.CDESTIP}</td>
                              <td>
                                 <input type="radio" value={item.CIDFOLD} />
                              </td>
                           </tr>
                        ))
                     ) : (
                        <td>NO HAY BIBLIOGRAFÍA REGISTRADA</td>
                     )}
                  </tbody>
               </table>
            </div>
            <br />
            <div className="row">
               <div className="col">
                  <a
                     className="btn btn-success"
                     href="/Fpg1120/Fpg1124"
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Agregar
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-success"
                     onClick={editar}
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Editar
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-danger"
                     href="/Fpg1120"
                     style={{ display: "block" }}
                  >
                     Salir
                  </a>
               </div>
            </div>
         </div>
      </div>
   );
};

export default Bibliografia;
