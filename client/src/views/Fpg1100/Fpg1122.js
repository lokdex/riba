import React, { useState } from "react";
import axios from "../../axios";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";
import { DevTool } from "@hookform/devtools";

const ParticipanteForm = ({ location }) => {
   const navigate = useNavigate();
   const { control, handleSubmit, register } = useForm({
      defaultValues: { CIDFOLD: location?.state?.CIDFOLD },
   });

   const [loading, setLoading] = useState(false);

   const [inputList, setInputList] = useState([
      { cnrodni: "", ctipo: "", nnota: "", cpartic: "" },
   ]);

   const onSubmit = async (params) => {
      console.log({ ...params, CUSUCOD: "1221" });
      setLoading(true);
      try {
         let response = await axios.post("/FPG1610P", {
            ADATOS: [params],
            CUSUCOD: "1221",
            CIDFOLD: location?.state?.CIDFOLD,
         });
         console.log(response.data);
         alert.info({
            title: "Info",
            content: "El registro fue actualizado",
         });
         navigate("/Fpg1121");
      } catch (err) {
         if (err.response) {
            alert({
               title: "Error",
               content: err.response.data,
            });
         } else if (err.request) {
            alert({
               title: "Error",
               content: "Error de red",
            });
         } else {
            alert({
               title: "Error",
               content: "Error desconocido",
            });
         }
      } finally {
         setLoading(false);
      }
   };
   const FormInput = ({ tag, label, ...props }) => (
      <FormBase tag={tag} label={label}>
         <input
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         />
      </FormBase>
   );

   const FormSelect = ({
      tag,
      label,
      options,
      option: { value, text },
      ...props
   }) => (
      <FormBase tag={tag} label={label}>
         <select
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         >
            {options.map((option) => (
               <option value={option[value]}>{option[text]}</option>
            ))}
         </select>
      </FormBase>
   );

   const FormBase = ({ tag, label, children }) => (
      <div className="form-group row">
         <label htmlFor={tag} class="col-sm-2 col-form-label">
            {label}
         </label>
         <div class="col-sm-10">{children}</div>
      </div>
   );

   const handleInputChange = (e, index) => {
      const { name, value } = e.target;
      const list = inputList;
      list[index][name] = value;
      setInputList(list);
   };

   const handleRemove = (index) => {
      const list = [...inputList];
      list.splice(index, 1);
      setInputList(list);
   };

   const handleAdd = () => {
      setInputList([
         ...inputList,
         { cnrodni: "", ctipo: "", nnota: "", cpartic: "" },
      ]);
      document.getElementById("cnrodni").value = "";
      document.getElementById("ctipo").value = "-";
      document.getElementById("nnota").value = "";
      document.getElementById("cpartic").value = "";
   };

   return (
      <>
         <div className="container">
            <table className="table table-striped">
               <thead>
                  <tr>
                     <th scope="col">NRODNI</th>
                     <th scope="col">TIPO</th>
                     <th scope="col">NOTA</th>
                     <th scope="col">% PART.</th>
                     <th scope="col"></th>
                  </tr>
               </thead>
               <tbody>
                  {inputList.map((participante, i) => {
                     if (inputList[i]["cnrodni"] != "")
                        return (
                           <tr>
                              <td>{participante.cnrodni}</td>
                              <td>{participante.ctipo}</td>
                              <td>{participante.nnota}</td>
                              <td>{participante.cpartic}</td>
                              <td>
                                 <button onClick={() => handleRemove(i)}>
                                    Eliminar
                                 </button>
                              </td>
                           </tr>
                        );
                  })}
               </tbody>
            </table>
         </div>

         <div className="container">
            <div className="card mt-3">
               <div className="card-header mb-3" style={styles.header}>
                  <h2>{"Crear"} Participante</h2>
               </div>
               <div className="container-fluid">
                  <form onSubmit={handleSubmit(onSubmit)}>
                     <FormInput tag="NSERIAL" label="ID" readOnly />
                     <FormInput
                        tag="CIDFOLD"
                        label="Folder"
                        value={location?.state?.CIDFOLD}
                     />
                     <div className="form-group">
                        <label>DNI</label>
                        <input
                           className="form-control"
                           tag="CNRODNI"
                           name="cnrodni"
                           id="cnrodni"
                           placeholder="Ingrese DNI del participante"
                           label="DNI"
                           onChange={(e) =>
                              handleInputChange(e, inputList.length - 1)
                           }
                        />
                     </div>
                     <div className="form-group">
                        <label>Tipo de participante</label>
                        <select
                           className="form-control"
                           tag="CTIPO"
                           name="ctipo"
                           id="ctipo"
                           label="Tipo"
                           onChange={(e) =>
                              handleInputChange(e, inputList.length - 1)
                           }
                        >
                           <option value="-">-</option>
                           <option value="D">DOCENTE</option>
                           <option value="S">ASESOR</option>
                           <option value="A">ALUMNO</option>
                        </select>
                     </div>
                     <div className="form-group">
                        <label>Nota</label>
                        <input
                           className="form-control"
                           tag="NNOTA"
                           name="nnota"
                           id="nnota"
                           placeholder="Ingrese nota del alumno, caso contrario -1"
                           label="NOTA"
                           onChange={(e) =>
                              handleInputChange(e, inputList.length - 1)
                           }
                        />
                     </div>
                     <div className="form-group">
                        <label>Porcentaje de participación</label>
                        <input
                           className="form-control"
                           tag="NPORPAR"
                           name="cpartic"
                           id="cpartic"
                           placeholder="Ingrese porcentaje de participación"
                           label="% Participación"
                           onChange={(e) =>
                              handleInputChange(e, inputList.length - 1)
                           }
                        />
                     </div>
                     <div className="row">
                        <div className="col">
                           <input
                              type="submit"
                              value="Guardar"
                              className="btn btn-success btn-block"
                              style={{
                                 backgroundColor: "#245433",
                              }}
                           />
                        </div>
                        <div className="col">
                           <button
                              type="button"
                              className="btn btn-success btn-block"
                              onClick={handleAdd}
                              style={{
                                 backgroundColor: "#245433",
                              }}
                           >
                              Añadir
                           </button>
                        </div>
                        <div className="col">
                           <Link to="/Fpg1121">
                              <button className="btn btn-danger btn-block">
                                 Cancelar
                              </button>
                           </Link>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <DevTool control={control} />
         </div>
      </>
   );
};

const styles = {
   error: {
      color: "red",
      margin: 0,
   },
   header: {
      backgroundColor: "#f8AA1A",
      textAlign: "center",
      fontWeight: "bold",
   },
   btn: {
      backgroundColor: "#245433",
   },
};

export default ParticipanteForm;
