import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate, Link } from "@reach/router";
import { DevTool } from "@hookform/devtools";
import { ErrorMessage } from "@hookform/error-message";

import axios from "../../axios";

const UpdateForm = (props) => {
   const navigate = useNavigate();
   console.log(props?.location?.state);
   const { control, handleSubmit, errors, register } = useForm({
      defaultValues: props.location.state,
   });

   let [ATIPFOL, setATIPFOL] = useState([]);
   let [ATIPCUR, setATIPCUR] = useState([]);
   let [ACODDOC, setACODDOC] = useState([]);
   let [ACODCUR, setACODCUR] = useState([]);
   let [AEVENTO, setAEVENTO] = useState([]);
   /*
   const [init, setInit] = useState({
      ATIPFOL: [],
      ATIPCUR: [],
      ACODDOC: [],
      ACODCUR: [],
      AEVENTO: [],
   });
   */

   let initFolders = async () => {
      try {
         let response = await axios.post("/FPG1610I", {
            CUSUCOD: "1221",
            //CUNIACA: "40",
         });
         if (response?.data) {
            setATIPFOL(response.data.ATIPFOL);
            setATIPCUR(response.data.ATIPCUR);
            setACODDOC(response.data.ACODDOC);
            setACODCUR(response.data.ACODCUR);
            setAEVENTO(response.data.AEVENTO);
         }
      } catch (error) {
         alert("Error al cargar datos");
      }
   };

   let onSubmit = async (formData) => {
      console.log(formData);
      try {
         let response = await axios.post("/FPG1610G", {
            ...formData,
            CUSUCOD: "1221",
         });
         alert("El registro fue guardado");
      } catch (error) {
         alert("Error al guardar");
      }
   };

   useEffect(() => {
      initFolders();
   }, []);

   const participantes = () => {
      //if (selected){console.log(typeof selected['0']);}
      //console.log({ state: selected['0'] });
      navigate("/participantes", { state: { CIDFOLD: props?.location?.state?.CIDFOLD } });
   };

   const bibliografia = () => {
      //if (selected){console.log(typeof selected['0']);}
      //console.log({ state: selected['0'] });
      //navigate("/participantes", { state: { CIDFOLD: props?.location?.state?.CIDFOLD } });
   };

   const FormInput = ({ tag, label, ...props }) => (
      <FormBase tag={tag} label={label}>
         <input
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         />
      </FormBase>
   );

   const FormTextArea = ({ tag, label, ...props }) => (
      <FormBase tag={tag} label={label}>
         <textarea
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         />
      </FormBase>
   );

   const FormSelect = ({
      tag,
      label,
      options,
      option: { value, text },
      ...props
   }) => (
      <FormBase tag={tag} label={label}>
         <select
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         >
            {options.map((option) => (
               <option value={option[value]}>{option[text]}</option>
            ))}
         </select>
      </FormBase>
   );

   const FormBase = ({ tag, label, children }) => (
      <div className="form-group row">
         <label htmlFor={tag} class="col-sm-2 col-form-label">
            {label}
         </label>
         <div class="col-sm-10">{children}</div>
      </div>
   );

   return (
      <div className="container">
         <div className="card mt-3 mb-5">
            <div className="container-fluid">
               <div
                  className="row"
                  style={{
                     color: "white",
                     backgroundColor: "rgb(71,145,77)",
                  }}
               >
                  <div className="col text-center p-3">
                     <h2>{"Editar"} Folder</h2>
                  </div>
               </div>

               <form onSubmit={handleSubmit(onSubmit)}>
                  <FormInput tag="CIDFOLD" label="ID" readOnly />
                  <FormSelect
                     tag="CTIPO"
                     label="Tipo"
                     options={ATIPFOL}
                     option={{ value: "CCODIGO", text: "CDESCRI" }}
                  />
                  <FormSelect
                     tag="CESTADO"
                     label="Estado"
                     options={[
                        { value: "A", text: "ACTIVO" },
                        { value: "I", text: "INACTIVO" },
                     ]}
                     option={{ value: "value", text: "text" }}
                  />
                  <FormInput tag="CPROYEC" label="Proyec. Academico" />
                  <FormSelect
                     tag="CCODDOC"
                     label="Docente"
                     options={ACODDOC}
                     option={{ value: "CCODDOC", text: "CNOMBRE" }}
                  />
                  <FormSelect
                     tag="CCODCUR"
                     label="Curso"
                     options={ACODCUR}
                     option={{ value: "CCODCUR", text: "CDESCRI" }}
                  />
                  <FormSelect
                     tag="CTIPCUR"
                     label="Tipo de Curso"
                     options={ATIPCUR}
                     option={{ value: "CCODIGO", text: "CDESCRI" }}
                  />

                  <FormInput tag="CGRUSEC" label="Grupo / Seccion" />

                  <FormInput tag="CFASE" label="Fase" />
                  <FormInput tag="CSUBFAS" label="Subfase" />
                  <FormInput tag="DFECHA" label="Fecha" />
                  <FormTextArea tag="MTITULO" label="Titulo" rows={2} />
                  <FormTextArea tag="MLINK" label="Link" rows={2} />
                  <FormInput tag="CEVALUA" label="Evaluacion" />

                  <FormSelect
                     tag="CEVENTO"
                     label="Tipo de Curso"
                     options={AEVENTO}
                     option={{ value: "CEVENTO", text: "CDESTIP" }}
                  />
               </form>
            </div>
            <br />
            <div className="row">
               <div className="col">
                  <input
                     type="submit"
                     value="Guardar"
                     className="btn btn-success"
                     style={{ backgroundColor: "#245433", display: "block" }}
                  />
               </div>
               <div className="col">
                  <a
                     className="btn btn-success"
                     onClick={participantes}
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Participantes
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-success"
                     onClick={bibliografia}
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Bibliografía
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-success"
                     href="/documento"
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Archivos
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-danger"
                     href="/"
                     style={{ display: "block" }}
                  >
                     Salir
                  </a>
               </div>
            </div>
         </div>
         <DevTool control={control} />
      </div>
   );
};

export default UpdateForm;
