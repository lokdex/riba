import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "@reach/router";
import { DevTool } from "@hookform/devtools";

import axios from "../../axios";

const Fpg1120 = () => {
   let navigate = useNavigate();
   let initJson = sessionStorage.getItem("OINIT");
   let folderJson = sessionStorage.getItem("OFOLDER");

   let [state, setState] = useState({});
   let init = initJson
      ? JSON.parse(initJson)
      : (() => {
           alert("Error al inicializar datos");
           navigate("/Fpg1110");
        })();

   const { register, errors, control, handleSubmit } = useForm({
      defaultValues: folderJson
         ? JSON.parse(folderJson)
         : {
              CIDFOLD: "*",
           },
   });

   const onSubmit = async (formData) => {
      setState(formData);
      try {
         let response = await axios.post("/FPG1610G", {
            ...formData,
            CCODUSU: sessionStorage.getItem("CCODUSU"),
         });
         alert("El Folder fue guardado");
         sessionStorage.removeItem("CIDFOLD");
         sessionStorage.removeItem("OFOLDER");
         navigate("/Fpg1110");
      } catch (e) {
         alert("Error al guardar Folder");
      }
   };

   return (
      <>
         <div className="container">
            <div className="card">
               <div className="card-header" style={styles.header}>
                  <h2>
                     {sessionStorage.getItem("CIDFOLD") ? "Editar" : "Crear"}{" "}
                     Folder
                  </h2>
               </div>

               <form
                  onSubmit={handleSubmit(onSubmit)}
                  className="container-fluid pt-3"
               >
                  <div className="form-group row">
                     <label
                        htmlFor="CIDFOLD"
                        className="col-sm-2 col-form-label"
                     >
                        ID
                     </label>
                     <div className="col-sm-10">
                        <input
                           type="text"
                           name="CIDFOLD"
                           id="CIDFOLD"
                           ref={register(validation.CIDFOLD)}
                           className="form-control"
                           readOnly
                        />
                        {<p style={styles.error}>{errors?.CIDFOLD?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label htmlFor="CTIPO" className="col-sm-2 col-form-label">
                        Tipo
                     </label>
                     <div className="col-sm-10">
                        <select
                           name="CTIPO"
                           id="CTIPO"
                           ref={register(validation.CTIPO)}
                           className="form-control"
                        >
                           {init?.ATIPFOL?.map((option) => (
                              <option value={option.CCODIGO}>
                                 {option.CDESCRI}
                              </option>
                           ))}
                        </select>
                        {<p style={styles.error}>{errors?.CTIPO?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CESTADO"
                        className="col-sm-2 col-form-label"
                     >
                        Estado
                     </label>
                     <div className="col-sm-10">
                        <select
                           name="CESTADO"
                           id="CESTADO"
                           ref={register(validation.CESTADO)}
                           className="form-control"
                        >
                           <option value="A">ACTIVO</option>
                           <option value="I">INACTIVO</option>
                        </select>
                        {<p style={styles.error}>{errors?.CESTADO?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CPROYEC"
                        className="col-sm-2 col-form-label"
                     >
                        Proyec. Academico
                     </label>
                     <div className="col-sm-10">
                        <input
                           type="text"
                           name="CPROYEC"
                           id="CPROYEC"
                           ref={register(validation.CPROYEC)}
                           className="form-control"
                        />
                        {<p style={styles.error}>{errors?.CPROYEC?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CCODDOC"
                        className="col-sm-2 col-form-label"
                     >
                        Docente
                     </label>
                     <div className="col-sm-10">
                        <select
                           name="CCODDOC"
                           id="CCODDOC"
                           ref={register(validation.CCODDOC)}
                           className="form-control"
                        >
                           {init?.ACODDOC?.map((option) => (
                              <option value={option.CCODDOC}>
                                 {option.CNOMBRE}
                              </option>
                           ))}
                        </select>
                        {<p style={styles.error}>{errors?.CCODDOC?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CCODCUR"
                        className="col-sm-2 col-form-label"
                     >
                        Curso
                     </label>
                     <div className="col-sm-10">
                        <select
                           name="CCODCUR"
                           id="CCODCUR"
                           ref={register(validation.CCODCUR)}
                           className="form-control"
                        >
                           {init?.ACODCUR?.map((option) => (
                              <option value={option.CCODCUR}>
                                 {option.CDESCRI}
                              </option>
                           ))}
                        </select>
                        {<p style={styles.error}>{errors?.CTIPO?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CTIPCUR"
                        className="col-sm-2 col-form-label"
                     >
                        Tipo de Curso
                     </label>
                     <div className="col-sm-10">
                        <select
                           name="CTIPCUR"
                           id="CTIPCUR"
                           ref={register(validation.CTIPCUR)}
                           className="form-control"
                        >
                           {init?.ATIPCUR?.map((option) => (
                              <option value={option.CCODIGO}>
                                 {option.CDESCRI}
                              </option>
                           ))}
                        </select>
                        {<p style={styles.error}>{errors?.CTIPCUR?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CGRUSEC"
                        className="col-sm-2 col-form-label"
                     >
                        Grupo / Seccion
                     </label>
                     <div className="col-sm-10">
                        <input
                           type="text"
                           name="CGRUSEC"
                           id="CGRUSEC"
                           ref={register(validation.CGRUSEC)}
                           className="form-control"
                        />
                        {<p style={styles.error}>{errors?.CGRUSEC?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label htmlFor="CFASE" className="col-sm-2 col-form-label">
                        Fase
                     </label>
                     <div className="col-sm-10">
                        <input
                           type="text"
                           name="CFASE"
                           id="CFASE"
                           ref={register(validation.CFASE)}
                           className="form-control"
                        />
                        {<p style={styles.error}>{errors?.CFASE?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CSUBFAS"
                        className="col-sm-2 col-form-label"
                     >
                        Subfase
                     </label>
                     <div className="col-sm-10">
                        <input
                           type="text"
                           name="CSUBFAS"
                           id="CSUBFAS"
                           ref={register(validation.CSUBFAS)}
                           className="form-control"
                        />
                        {<p style={styles.error}>{errors?.CSUBFAS?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="DFECHA"
                        className="col-sm-2 col-form-label"
                     >
                        Fecha
                     </label>
                     <div className="col-sm-10">
                        <input
                           type="text"
                           name="DFECHA"
                           id="DFECHA"
                           ref={register(validation.DFECHA)}
                           className="form-control"
                        />
                        {<p style={styles.error}>{errors?.DFECHA?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="MTITULO"
                        className="col-sm-2 col-form-label"
                     >
                        Titulo
                     </label>
                     <div className="col-sm-10">
                        <textarea
                           name="MTITULO"
                           id="MTITULO"
                           ref={register(validation.MTITULO)}
                           className="form-control"
                           rows={2}
                        />
                        {<p style={styles.error}>{errors?.MTITULO?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label htmlFor="MLINK" className="col-sm-2 col-form-label">
                        Link
                     </label>
                     <div className="col-sm-10">
                        <textarea
                           name="MLINK"
                           id="MLINK"
                           ref={register(validation.MLINK)}
                           className="form-control"
                           rows={2}
                        />
                        {<p style={styles.error}>{errors?.MLINK?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CEVALUA"
                        className="col-sm-2 col-form-label"
                     >
                        Evaluacion
                     </label>
                     <div className="col-sm-10">
                        <input
                           type="text"
                           name="CEVALUA"
                           id="CEVALUA"
                           ref={register(validation.CEVALUA)}
                           className="form-control"
                        />
                        {<p style={styles.error}>{errors?.CEVALUA?.message}</p>}
                     </div>
                  </div>

                  <div className="form-group row">
                     <label
                        htmlFor="CEVENTO"
                        className="col-sm-2 col-form-label"
                     >
                        Evento
                     </label>
                     <div className="col-sm-10">
                        <select
                           name="CEVENTO"
                           id="CEVENTO"
                           ref={register(validation.CEVENTO)}
                           className="form-control"
                        >
                           {init?.AEVENTO?.map((option) => (
                              <option value={option.CEVENTO}>
                                 {option.CDESTIP}
                              </option>
                           ))}
                        </select>
                        {<p style={styles.error}>{errors?.CEVENTO?.message}</p>}
                     </div>
                  </div>

                  <div className="row">
                     <div className="col">
                        <input
                           style={styles.btn}
                           type="submit"
                           value="Guardar"
                           className="btn btn-block btn-success"
                        />
                     </div>
                     {sessionStorage.getItem("CIDFOLD") && (
                        <>
                           <div className="col">
                              <a
                                 className="btn btn-success"
                                 href="/Fpg1121"
                                 style={{
                                    backgroundColor: "#245433",
                                    display: "block",
                                 }}
                              >
                                 Participantes
                              </a>
                           </div>
                           <div className="col">
                              <a
                                 className="btn btn-success"
                                 href="/Fpg1123"
                                 style={{
                                    backgroundColor: "#245433",
                                    display: "block",
                                 }}
                              >
                                 Bibliografía
                              </a>
                           </div>
                           <div className="col">
                              <a
                                 className="btn btn-success"
                                 href="/documento"
                                 style={{
                                    backgroundColor: "#245433",
                                    display: "block",
                                 }}
                              >
                                 Archivos
                              </a>
                           </div>
                        </>
                     )}
                     <div className="col">
                        <Link to="/Fpg1110">
                           <button className="btn btn-block btn-danger">
                              Cancelar
                           </button>
                        </Link>
                     </div>
                  </div>

                  <pre>{JSON.stringify(state, undefined, 4)}</pre>
               </form>
            </div>
         </div>
         <DevTool control={control} />
      </>
   );
};

const required = {
   value: true,
   message: "Este campo es obligatorio",
};

const validation = {
   CIDFOLD: {
      required,
   },
   CTIPO: {
      required,
   },
   CESTADO: {
      required,
   },
   CPROYEC: {
      required,
   },
   CCODDOC: {
      required,
   },
   CCODCUR: {
      required,
   },
   CTIPCUR: {
      required,
   },
   CGRUSEC: {
      required,
   },
   CFASE: {
      required,
   },
   CSUBFAS: {
      required,
   },
   DFECHA: {
      required,
   },
   MTITULO: {
      required,
   },
   MLINK: {
      required,
   },
   CEVALUA: {
      required,
   },
   CEVENTO: {
      required,
   },
};

const styles = {
   error: {
      color: "red",
      margin: 0,
   },
   header: {
      backgroundColor: "#f8AA1A",
      textAlign: "center",
      fontWeight: "bold",
   },
   btn: {
      backgroundColor: "#245433",
   },
};

export default Fpg1120;

/*
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { DevTool } from "@hookform/devtools";
import { ErrorMessage } from "@hookform/error-message";

import axios from "../../axios";

const FoldersForm = (props) => {
   let { register, control, errors, handleSubmit } = useForm({
      defaultValues: {
         CIDFOLD: "*",
      },
   });

   let [ATIPFOL, setATIPFOL] = useState([]);
   let [ATIPCUR, setATIPCUR] = useState([]);
   let [ACODDOC, setACODDOC] = useState([]);
   let [ACODCUR, setACODCUR] = useState([]);
   let [AEVENTO, setAEVENTO] = useState([]);
   
   const [init, setInit] = useState({
      ATIPFOL: [],
      ATIPCUR: [],
      ACODDOC: [],
      ACODCUR: [],
      AEVENTO: [],
   });
   

  let initFolders = async () => {
   try {
      let response = await axios.post("/FPG1610I", {
         CUSUCOD: "1221",
         //CUNIACA: "40",
      });
      if (response?.data) {
         setATIPFOL(response.data.ATIPFOL);
         setATIPCUR(response.data.ATIPCUR);
         setACODDOC(response.data.ACODDOC);
         setACODCUR(response.data.ACODCUR);
         setAEVENTO(response.data.AEVENTO);
      }
   } catch (error) {
      alert("Error al cargar datos");
   }
};

let onSubmit = async (formData) => {
   console.log(formData);
   try {
      let response = await axios.post("/FPG1610G", {
         ...formData,
         CUSUCOD: "1221",
      });
      alert("El registro fue guardado");
   } catch (error) {
      alert("Error al guardar");
   }
};

useEffect(() => {
   initFolders();
}, []);

const FormInput = ({ tag, label, ...props }) => (
   <FormBase tag={tag} label={label}>
      <input
         ref={register}
         name={tag}
         id={tag}
         className="form-control"
         {...props}
      />
   </FormBase>
);

const FormTextArea = ({ tag, label, ...props }) => (
   <FormBase tag={tag} label={label}>
      <textarea
         ref={register}
         name={tag}
         id={tag}
         className="form-control"
         {...props}
      />
   </FormBase>
);

const FormSelect = ({
   tag,
   label,
   options,
   option: { value, text },
   ...props
}) => (
   <FormBase tag={tag} label={label}>
      <select
         ref={register}
         name={tag}
         id={tag}
         className="form-control"
         {...props}
      >
         {options.map((option) => (
            <option value={option[value]}>{option[text]}</option>
         ))}
      </select>
   </FormBase>
);

const FormBase = ({ tag, label, children }) => (
   <div className="form-group row">
      <label htmlFor={tag} class="col-sm-2 col-form-label">
         {label}
      </label>
      <div class="col-sm-10">{children}</div>
   </div>
);

 

                    
                  <div className="col">
                     <a
                        className="btn btn-danger"
                        href="/Fpg1110"
                        style={{ display: "block" }}
                     >
                        Cancelar
                     </a>
                  </div>
               </div>
            </form>
         </div>
      </div>
      <DevTool control={control} />
   </div>
);
};

export default FoldersForm;

*/
