import React, { useState } from "react";
import axios from "../../axios";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";
import { DevTool } from "@hookform/devtools";

const BibliografiaForm = () => {
   let { register, control, errors, handleSubmit } = useForm({
      defaultValues: {},
   });

   let [AIDFOLD, setAIDFOLD] = useState([]);

   const navigate = useNavigate();

   const [loading, setLoading] = useState(false);

   const onSubmit = async (params) => {
      setLoading(true);
      try {
         let response = axios.post("/FPG1610B", { ...params, CUSUCOD: "1221" });
         console.log(response.data);
         alert.info({
            title: "Info",
            content: "El registro fue creado",
         });
         navigate("/bibliografia");
      } catch (err) {
         if (err.response) {
            alert.error({
               title: "Error",
               content: err.response.data,
            });
         } else if (err.request) {
            alert.error({
               title: "Error",
               content: "Error de red",
            });
         } else {
            alert.error({
               title: "Error",
               content: "Error desconocido",
            });
         }
      } finally {
         setLoading(false);
      }
   };

   const FormInput = ({ tag, label, ...props }) => (
      <FormBase tag={tag} label={label}>
         <input
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         />
      </FormBase>
   );

   const FormTextArea = ({ tag, label, ...props }) => (
      <FormBase tag={tag} label={label}>
         <textarea
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         />
      </FormBase>
   );

   const FormSelect = ({
      tag,
      label,
      options,
      option: { value, text },
      ...props
   }) => (
      <FormBase tag={tag} label={label}>
         <select
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         >
            {options.map((option) => (
               <option value={option[value]}>{option[text]}</option>
            ))}
         </select>
      </FormBase>
   );

   const FormBase = ({ tag, label, children }) => (
      <div className="form-group row">
         <label htmlFor={tag} class="col-sm-2 col-form-label">
            {label}
         </label>
         <div class="col-sm-10">{children}</div>
      </div>
   );
   return (
      <div className="container">
         <div className="card mt-3">
            <div className="container-fluid">
               <div
                  className="row"
                  style={{
                     color: "white",
                     backgroundColor: "rgb(71,145,77)",
                  }}
               >
                  <div className="col text-center p-3">
                     <h2>{"Crear"} Bibliografia</h2>
                  </div>
               </div>

               <form onSubmit={handleSubmit(onSubmit)}>
                  <FormInput tag="NSERIAL" label="ID" readOnly />
                  <FormInput tag="CIDFOLD" label="Folder" />
                  <FormSelect
                     tag="CTIPO"
                     label="Tipo"
                     options={[
                        { value: "L", text: "LIBRO" },
                        { value: "T", text: "TEXTO UNIVERSITARIO" },
                        { value: "E", text: "TESIS" },
                        { value: "M", text: "MONOGRAFIA" },
                        { value: "L", text: "ENLACE INTERNET" },
                     ]}
                     option={{ value: "value", text: "text" }}
                  />
                  <FormInput tag="CIDLIBR" label="ID Libro" />
                  <FormInput tag="CIDISBN" label="ISBN" />
                  <FormTextArea tag="MDATOS" label="Datos" rows={2} />

                  <input type="submit" value="Guardar" className="btn" />
                  <a
                     className="btn btn-danger"
                     href="/Fpg1120"
                     style={{ display: "block" }}
                  >
                     Salir
                  </a>
               </form>
            </div>
         </div>
         <DevTool control={control} />
      </div>
   );
};

export default BibliografiaForm;
