import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";

import axios from "../../axios";

const styles = {};

const Fpg1110 = () => {
   const navigate = useNavigate();
   const { control, handleSubmit, errors } = useForm();
   /*data fetch */
   const [selected, setSelected] = useState(null);
   const [data, setData] = useState([]);

   const documento = async (url, params = {}) => {
      try {
         let response = await axios.post("/documento");
         window.open(
            "http://localhost:8082/static" + response.data,
            "_blank",
            "toolbar=yes, scrollbars=yes, resizable=yes, width=950, height=650"
         );
      } catch (error) {
         alert("Error al subir documento");
      }
   };

   const cargarFolders = async () => {
      try {
         let response = await axios.post("/FPG1610F", {
            CUSUCOD: sessionStorage.getItem("CUSUCOD"),
         });
         if (response?.data) setData(response?.data);
      } catch (error) {
         alert("Error al cargar folders");
      }
   };

   useEffect(() => {
      cargarFolders("ConsultarFolders");
      sessionStorage.removeItem("CIDFOLD");
      sessionStorage.removeItem("OFOLDER");
   }, []);

   const editar = () => {
      sessionStorage.setItem("CIDFOLD", selected);
      data.forEach((dato) => {
         if (dato.CIDFOLD === selected)
            sessionStorage.setItem("OFOLDER", JSON.stringify(dato));
      });
      navigate("/Fpg1120");
   };

   return (
      <div className="container-fluid">
         <br />
         <div
            className="card"
            style={{
               margin: "100px",
            }}
         >
            <div className="card-body">
               <div
                  className="card-title"
                  style={{ fontSize: "20px", fontWeight: "500" }}
               >
                  Folders registrados
               </div>
               <table className="table table-striped">
                  <thead
                     style={{
                        color: "white",
                        backgroundColor: "rgb(71,145,77)",
                     }}
                  >
                     <tr>
                        <th scope="col">ID</th>
                        <th scope="col">TÍTULO</th>
                        <th scope="col">DOCENTE</th>
                        <th scope="col">CURSO</th>
                        <th scope="col">TIPO</th>
                        <th scope="col"></th>
                     </tr>
                  </thead>
                  <tbody>
                     {data.length ? (
                        data.map((item, idx) => (
                           <tr>
                              <td>{item.CIDFOLD}</td>
                              <td>{item.MTITULO}</td>
                              <td>{item.CNOMBRE}</td>
                              <td>{item.CDESCUR}</td>
                              <td>{item.CDESTIP}</td>
                              <td>
                                 <input
                                    type="radio"
                                    name="CIDFOLD"
                                    onChange={() => {
                                       setSelected(item.CIDFOLD);
                                    }}
                                 />
                              </td>
                           </tr>
                        ))
                     ) : (
                        <td>NO HAY FOLDERS REGISTRADOS</td>
                     )}
                  </tbody>
               </table>
            </div>
            <br />
            <div className="row">
               <div className="col">
                  <Link to="/Fpg1120">
                     <button
                        className="btn btn-success btn-block"
                        style={{ backgroundColor: "#245433"}}
                     >
                        Crear Folder
                     </button>
                  </Link>
               </div>
               <div className="col">
                  <button
                     className="btn btn-success btn-block"
                     onClick={editar}
                     style={{ backgroundColor: "#245433"}}
                  >
                     Editar Folder
                  </button>
               </div>
               <div className="col">
                  <Link to="/">
                     <button
                        className="btn btn-danger btn-block"
                     >
                        Salir
                     </button>
                  </Link>
               </div>
            </div>
         </div>
      </div>
   );
};

export default Fpg1110;
