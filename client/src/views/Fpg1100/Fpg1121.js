import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";

import axios from "../../axios";

const Participantes = (props) => {
   console.log(props?.location?.state);
   const navigate = useNavigate();
   const { control, handleSubmit, errors } = useForm();
   /*data fetch */
   const [selected, setSelected] = useState(null);
   const [folder, setFolder] = useState(null);
   const [participantes, setParticipantes] = useState([]);

   const cargarParticipantes = async () => {
      try {
         let response = await axios.post("/FPG1610A", {
            CUSUCOD: "1221",
            CIDFOLD: sessionStorage.getItem("CIDFOLD"),
         });
         setParticipantes(response.data["DATOS"]);
         setFolder(response.data["DATA"]);
      } catch (error) {
         alert("Error al cargar participantes");
      }
   };

   useEffect(() => {
      cargarParticipantes();
   }, []);

   const editar = () => {
      let seleccionado;
      participantes.forEach((participante) => {
         if (participante.NSERIAL == selected) seleccionado = participante;
      });
      navigate("/Fpg1120/Fpg1122", { state: seleccionado });
   };

   const grabarParticipante = () => {
      navigate("/Fpg1122", {
         state: { CIDFOLD: props.location?.state?.CIDFOLD },
      });
   };

   return (
      <div className="container-fluid">
         <br />
         <div
            className="card"
            style={{
               margin: "100px",
            }}
         >
            <div className="card-body">
               <div
                  className="card-header"
                  style={styles.header}
               >
                  Participantes registrados -{" "}
                  {JSON.parse(sessionStorage.getItem("OFOLDER")).MTITULO}
               </div>
               <table className="table table-striped">
                  <thead
                     style={{
                        color: "white",
                        backgroundColor: "rgb(71,145,77)",
                     }}
                  >
                     <tr>
                        <th scope="col">NOMBRE</th>
                        <th scope="col">TIPO</th>
                        <th scope="col">NOTA</th>
                        <th scope="col">% DE PAR.</th>
                        <th scope="col"></th>
                     </tr>
                  </thead>
                  <tbody>
                     {participantes.length ? (
                        participantes.map((item, idx) => (
                           <tr>
                              <td>{item.CNOMBRE}</td>
                              <td>{item.CTIPO}</td>
                              <td>{item.NNOTA}</td>
                              <td>{item.NPORPAR}</td>
                              <td>
                                 <input type="radio" value={item.NSERIAL} />
                              </td>
                           </tr>
                        ))
                     ) : (
                        <td>NO HAY PARTICIPANTES REGISTRADOS</td>
                     )}
                  </tbody>
               </table>
            </div>
            <br />
            <div className="row">
               <div className="col">
                  <a
                     className="btn btn-success"
                     onClick={grabarParticipante}
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Nuevo
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-success"
                     onClick={editar}
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Editar
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-danger"
                     href="/Fpg1120"
                     style={{ display: "block" }}
                  >
                     Salir
                  </a>
               </div>
            </div>
         </div>
      </div>
   );
};

const styles = {
   error: {
      color: "red",
      margin: 0,
   },
   header: {
      backgroundColor: "#f8AA1A",
      textAlign: "center",
      fontWeight: "bold",
   },
   btn: {
      backgroundColor: "#245433",
   },
};

export default Participantes;
