import React, { useState } from "react";
import axios from "../axios";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";

const SubidaDocumento = (props) => {
   const [loading, setLoading] = useState(false);
   const [imagen, setImagen] = useState(null);

   const onSubmit = async (e) => {
      e.preventDefault();

      const formData = new FormData();
      // Update the formData object
      formData.append("myFile", imagen, imagen.name);
      console.log(imagen.name);
      setLoading(true);
      try {
         let response = await axios.post("/FPG1610S", formData);
      } catch (err) {
         console.log(err);
         if (err.response) {
            alert({
               title: "Error",
               content: err.response.data,
            });
         } else if (err.request) {
            alert({
               title: "Error",
               content: "Error de red",
            });
         } else {
            alert({
               title: "Error",
               content: "Error desconocido",
            });
         }
      } finally {
         setLoading(false);
      }
   };

   return (
      <form onSubmit={onSubmit} encType="multipart/form-data">
         <input
            onChange={(e) => setImagen(e.target.files[0])}
            type="file"
            name="file"
         />
         <input type="submit" value="Upload" className='btn btn-success'/>
      </form>
   );
};

export default SubidaDocumento;
