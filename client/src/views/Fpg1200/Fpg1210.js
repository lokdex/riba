import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";

import axios from "../../axios";

const styles = {};

const Cursos = () => {
   const navigate = useNavigate();
   const { control, handleSubmit, errors } = useForm();
   /*data fetch */
   const [selected, setSelected] = useState(null);
   const [data, setData] = useState([]);
   const [loading, setLoading] = useState(false);
   const [error, setError] = useState();
   let [ACODCUR, setACODCUR] = useState([]);
   const [plan, setPlan] = useState(null);
   const [semestre, setSemestre] = useState(null);

   const documento = async (url, params = {}) => {
      try {
         setLoading(true);
         setError(null);
         let response = await axios.post("/documento");
         window.open(
            "http://localhost:8082/static" + response.data,
            "_blank",
            "toolbar=yes, scrollbars=yes, resizable=yes, width=950, height=650"
         );
      } catch (error) {
         if (error.response) {
            setError(error.response.data);
         } else if (error.request) {
            setError("Error de red");
         } else {
            setError("Error desconocido");
         }
         alert(error);
      } finally {
         setLoading(false);
      }
   };

   let consultarCursos = async () => {
      try {
         let response = await axios.post("/FPG1610I", {
            CUSUCOD: "1221",
            //CUNIACA: "40",
         });
         if (response?.data) {
            setACODCUR(response.data.ACODCUR);
         }
      } catch (error) {
         alert("Error al cargar datos");
      }
   };

   useEffect(() => {
      consultarCursos();
   }, []);

   useEffect(() => {
      error &&
         alert({
            title: "Error",
            content: error,
         });
   }, [error]);

   const editar = () => {
      let seleccionado;
      data.forEach((dato) => {
         if (dato.CIDFOLD === selected) seleccionado = dato;
      });
      navigate("folders/update", { state: seleccionado });
   };

   const handleSelectPlan = ({target}) => {
      setPlan(target.value);
   };
   const handleSelectSemestre = ({target}) => {
      setSemestre(target.value);
   };

   return (
      <div className="container-fluid">
         <br />
         <div className="card">
            <div className="card-body">
               <div
                  className="card-title"
                  style={{ fontSize: "20px", fontWeight: "500" }}
               >
                  Seleccione plan de estudios
               </div>
               <div class="form-group">
                  <select
                     class="form-control"
                     id="select-plan-estudios"
                     onChange={handleSelectPlan}
                  >
                     <option>-</option>
                     <option value="41200204">2002</option>
                     <option value="41200904">2009</option>
                     <option value="41201605">2016</option>
                     <option value="41ALAS04">ALAS</option>
                  </select>
               </div>
               <div
                  className="card-title"
                  style={{ fontSize: "20px", fontWeight: "500" }}
               >
                  Seleccione semestre base
               </div>
               <div class="form-group">
                  <select
                     class="form-control"
                     id="select-semestre"
                     onChange={handleSelectSemestre}
                  >
                     <option>-</option>
                     <option value="01">1</option>
                     <option value="02">2</option>
                     <option value="03">3</option>
                     <option value="04">4</option>
                     <option value="05">5</option>
                     <option value="06">6</option>
                     <option value="07">7</option>
                     <option value="08">8</option>
                     <option value="09">9</option>
                     <option value="10">10</option>
                  </select>
               </div>
            </div>
         </div>
         <div
            className="card"
            style={{
               margin: "100px",
            }}
         >
            <div className="card-body">
               <div
                  className="card-title"
                  style={{ fontSize: "20px", fontWeight: "500" }}
               >
                  Cursos registrados
               </div>
               <table className="table table-striped">
                  <thead
                     style={{
                        color: "white",
                        backgroundColor: "rgb(71,145,77)",
                     }}
                  >
                     <tr>
                        <th scope="col">CÓDIGO</th>
                        <th scope="col">DESCRIPCIÓN</th>
                        <th scope='col'>ESTADO</th>
                        <th scope="col"></th>
                     </tr>
                  </thead>
                  <tbody>
                     {ACODCUR.length ? (
                        ACODCUR.map((curso, idx) => {
                           if (
                              curso.CPLAEST == "41ALAS04" &&
                              curso.CCODCUR.substring(4, 6) == semestre
                           )
                              return (
                                 <tr>
                                    <td>{curso.CCODCUR}</td>
                                    <td>{curso.CDESCRI}</td>
                                    <td>{curso.CESTADO}</td>
                                    <td>
                                       <input
                                          type="radio"
                                          onChange={() => {
                                             setSelected(curso.CCODCUR);
                                          }}
                                       />
                                    </td>
                                 </tr>
                              );
                           if (
                              curso.CPLAEST == plan &&
                              curso.CCODCUR.substring(2, 4) == semestre
                           )
                              return (
                                 <tr>
                                    <td>{curso.CCODCUR}</td>
                                    <td>{curso.CDESCRI}</td>
                                    <td>{curso.CESTADO}</td>
                                    <td>
                                       <input
                                          type="radio"
                                          onChange={() => {
                                             setSelected(curso.CCODCUR);
                                          }}
                                       />
                                    </td>
                                 </tr>
                              );
                        })
                     ) : (
                        <td>NO HAY CURSOS REGISTRADOS</td>
                     )}
                  </tbody>
               </table>
            </div>
            <br />
            <div className="row">
               <div className="col">
                  <a
                     className="btn btn-success"
                     href="folders/create"
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Subir documentos
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-success"
                     onClick={editar}
                     style={{ backgroundColor: "#245433", display: "block" }}
                  >
                     Examenes
                  </a>
               </div>
               <div className="col">
                  <a
                     className="btn btn-danger"
                     href="/"
                     style={{ display: "block" }}
                  >
                     Salir
                  </a>
               </div>
            </div>
         </div>
      </div>
   );
};

export default Cursos;
