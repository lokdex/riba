import React from "react";
import { useForm } from "react-hook-form";
import axios from "../../axios";

const styles = {
   grnBtn: {
      backgroundColor: "#245433",
      display: "block",
   },
   title: {
      color: "black",
      backgroundColor: "rgb(255,188,0)",
   },
};

const CursoDocs = ({ cursoId }) => {
   const { handleSubmit, register } = useForm();

   const onSubmit = async (formData) => {
      try {
         let response = await axios.post("", {});
         console.log("Archivos guardados");
      } catch (e) {
         console.log(e);
      }
   };

   const FormBase = ({ tag, label, children }) => (
      <div className="form-group row">
         <label htmlFor={tag} class="col-sm-2 col-form-label">
            {label}
         </label>
         <div class="col-sm-10">{children}</div>
      </div>
   );

   const FormInput = ({ tag, label, ...props }) => (
      <FormBase tag={tag} label={label}>
         <input
            ref={register}
            name={tag}
            id={tag}
            className="form-control"
            {...props}
         />
      </FormBase>
   );

   return (
      <div className="container">
         <div className="card p-3">
            <div className="row mb-3" style={styles.title}>
               <div className="col text-center p-3">
                  <h2>Subir Documentos del Curso</h2>
               </div>
            </div>
            <form
               onSubmit={handleSubmit(onSubmit)}
               encType="multipart/form-data"
            >
               <FormInput tag="SILABO" label="Silabo" type="file" />
               <FormInput tag="RUBRICA" label="Rubrica" type="file" />
               <FormInput tag="TALLER" label="Taller" type="file" />
               <input
                  type="submit"
                  value="Guardar"
                  className="btn px-5 btn-success"
                  style={styles.grnBtn}
               />
            </form>
         </div>
      </div>
   );
};

export default CursoDocs;
