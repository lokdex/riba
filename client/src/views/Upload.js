import React, { useState } from "react";
import axios from "../axios";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";
import { DevTool } from "@hookform/devtools";

const ParticipanteForm = ({ location }) => {
   const navigate = useNavigate();
   const { control, handleSubmit, register } = useForm();

   const [imagen, setImagen] = useState(null);

   const onSubmit = async () => {
      const formData = new FormData();
      formData.append("myFile", imagen, imagen.name);
      try {
         let response = await axios.post("/FPG1610S", formData);
         alert("El archivo fue subido");
      } catch (err) {
         console.log(err);
         if (err.response) {
            alert(err.response.data);
         } else if (err.request) {
            alert("Error de red");
         } else {
            alert("Error desconocido");
         }
      }
   };

   const FormInput = ({ tag, label, ...props }) => (
      <FormBase tag={tag} label={label}>
         <input name={tag} id={tag} className="form-control" {...props} />
      </FormBase>
   );

   const FormBase = ({ tag, label, children }) => (
      <div className="form-group row">
         <label htmlFor={tag} class="col-sm-2 col-form-label">
            {label}
         </label>
         <div class="col-sm-10">{children}</div>
      </div>
   );

   return (
      <div className="container">
         <div className="card mt-3">
            <div className="container-fluid">
               <div
                  className="row"
                  style={{
                     color: "white",
                     backgroundColor: "rgb(71,145,77)",
                  }}
               >
                  <div className="col text-center p-3">
                     <h2>Subir Archivo</h2>
                  </div>
               </div>

               <form
                  onSubmit={handleSubmit(onSubmit)}
                  encType="multipart/form-data"
               >
                  <div className="form-group row">
                     <label htmlFor="file" class="col-sm-2 col-form-label">
                        Archivo
                     </label>
                     <div class="col-sm-10">
                        <input
                           type="file"
                           name="file"
                           className="btn"
                           onChange={(e) => setImagen(e.target.files[0])}
                        />
                     </div>
                  </div>

                  <input type="submit" value="Subir" className="btn" />
                  <button className="btn btn-danger">Cancelar</button>
               </form>
            </div>
         </div>
         <DevTool control={control} />
      </div>
   );
};

export default ParticipanteForm;
