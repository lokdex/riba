import React, {useState} from "react";
import {useNavigate} from "@reach/router";
import {Controller, useForm} from "react-hook-form";
import {DevTool} from "@hookform/devtools";



import axios from "../axios";

const styles = {};


const Eventos = () => {
    const navigate = useNavigate();
    const {control, handleSubmit, errors} = useForm();
    /*data fetch */
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    
    const CargarEventos = () => {
        axios({
            method: "post",
            data: {query: 'Consultar'},
        })
           .then((response) => {
               console.log(response.data);
               setData(response.data);
               //console.log(data);
           })
           .catch((error) => {
               setError(error);
               alert.error({
                   title: "Error",
                   content: "Mensaje de Error",
               });
           })
           .finally(() => {
               setLoading(false);
           });
    }
    
    const onSubmit = (params) => {
        setData(null);
        setLoading(true);
        setError(null);
        console.log(params);
        axios({
            method: "post",
            data: {...params, inicio: params.periodo[0], finali: params.periodo[1], query: 'AgregarEvento'},
        })
            .then((response) => {
                setData(response.data);
                navigate("login");
            })
            .catch((error) => {
                setError(error);
                alert.error({
                    title: "Error",
                    content: "Mensaje de Error",
                });
            })
            .finally(() => {
                setLoading(false);
            });
    };


    const required = {
        value: true,
        message: "Este campo es obligatorio",
    };

    const validation = {
        evento: {
            required,
            minLength: {
                value: 4,
                message: "Debe tener una longitud de 4",
            },
            maxLength: {
                value: 4,
                message: "Debe tener una longitud de 4",
            },
        },
        estado: {required},
        tipo: {required},
        descri: {
            required,
            maxLength: {
                value: 200,
                message: "Debe tener menos de 200 caracteres",
            },
        },
        periodo: {required},
        lugar: {
            required,
            maxLength: {
                value: 100,
                message: "Debe tener menos de 100 caracteres",
            },
        },
    };

    return (
        <>
            {/* <Form
                labelCol={{span: 8}}
                wrapperCol={{span: 8}}
                onFinish={handleSubmit(onSubmit)}
            >
                <Row justify="center"><Typography.Title>Crear Eventos</Typography.Title></Row>

                <Form.Item
                    label="ID"
                    name="evento"
                    validateStatus={errors.evento && "error"}
                    help={errors?.evento?.message}
                >
                    <Controller
                        as={Input}
                        name="paData['CIDFOLD']"
                        control={control}
                        rules={validation.evento}
                    />
                </Form.Item>

                <Form.Item
                    label="Estado"
                    name="estado"
                    validateStatus={errors.estado && "error"}
                    help={errors?.estado?.message}
                >
                    <Controller
                        as={Select}
                        name="paData['CESTADO']"
                        control={control}
                        rules={validation.estado}
                        options={[
                            {label: "ACTIVO", value: "A"},
                            {label: "INACTIVO", value: "I"}
                        ]}
                    />
                </Form.Item>

                <Form.Item
                    label="Tipo"
                    name="tipo"
                    validateStatus={errors.tipo && "error"}
                    help={errors?.tipo?.message}
                >
                    <Controller
                        as={Select}
                        name="paData['CTIPO']"
                        control={control}
                        rules={validation.tipo}
                        options={[
                            {label: "CURSO", value: "C"},
                            {label: "CONGRESO", value: "G"},
                            {label: "SEMINARIO", value: "S"},
                            {label: "JORNADA", value: "J"}
                        ]}
                    />
                </Form.Item>

                <Form.Item
                    label="Descripcion"
                    name="descri"
                    validateStatus={errors.descri && "error"}
                    help={errors?.descri?.message}
                >
                    <Controller
                        as={Input.TextArea}
                        name="descri"
                        control={control}
                        rules={validation.descri}
                        rows={3}
                    />
                </Form.Item>

                <Form.Item
                    label="Inicio - Fin"
                    name="periodo"
                    validateStatus={errors.periodo && "error"}
                    help={errors?.periodo?.message}
                >
                    <Controller
                        as={DatePicker.RangePicker}
                        name="periodo"
                        control={control}
                        rules={validation.descri}
                        format="DD-MM-YYYY"
                    />
                </Form.Item>

                <Form.Item
                    label="Lugar"
                    name="lugar"
                    validateStatus={errors.lugar && "error"}
                    help={errors?.lugar?.message}
                >
                    <Controller
                        as={Input.TextArea}
                        name="lugar"
                        control={control}
                        rules={validation.lugar}
                        rows={2}
                    />
                </Form.Item>

                <Form.Item wrapperCol={{offset: 8, span: 16}}>
                    <Button htmlType="submit" loading={loading}>
                        Guardar
                    </Button>
                </Form.Item>
                <Form.Item wrapperCol={{offset: 8, span: 16}}>
                    <Button onClick={CargarEventos} loading={loading}>
                        Recuperar
                    </Button>
                </Form.Item>
            </Form>
            <DevTool control={control}/> */}
        </>
    );
};

export default Eventos;
