import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "@reach/router";
import { Controller, useForm } from "react-hook-form";
import { DevTool } from "@hookform/devtools";

import axios from "../axios";

const styles = {};

const Dashboard = () => {
   const navigate = useNavigate();

   return (
      <div className="container">
         <div
            className="row justify-content-center align-middle"
            style={{ paddingTop: "100px", paddingBottom: "100px" }}
         >
            <div className="col-lg-4 col-md-4 col-sm-10">
               <div
                  className="card"
                  style={{ paddingTop: "1em", paddingBottom: "1em" }}
               >
                  <div className="col">
                     <a
                        className="btn btn-success btn-block"
                        href="/Fpg1110"
                        style={{ fontWeight: 500 }}
                     >
                        Mantenimiento de Folders
                     </a>
                  </div>
                  <br/>
                  <div className="col">
                     <a
                        className="btn btn-success btn-block"
                        href="/Fpg1210"
                        style={{ fontWeight: 500 }}
                     >
                        Mantenimiento de Cursos
                     </a>
                  </div>
                  <br/>
                  <div className="col">
                     <a
                        className="btn btn-success btn-block"
                        href="/eventos"
                        style={{ fontWeight: 500 }}
                     >
                        Mantenimiento de Eventos
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
};

export default Dashboard;
