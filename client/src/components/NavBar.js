import React from "react";
import { Link, useLocation } from "@reach/router";

import logoucsm from "../img/logo_ucsm.png";

const NavBar = () => {
   const location = useLocation();
   return (
      <nav
         className="navbar navbar-expand-md navbar-dark sticky-top"
         style={{ backgroundColor: "#245433", color: "#fff" }}
      >
         <img
            src={logoucsm}
            alt="logo UCSM"
            align="left"
            width="75"
            height="75"
         />
         <a
            className="navbar-brand"
            href="/"
            style={{ fontWeight: 500, fontSize: "25px" }}
         >
            UCSM - RIBA
         </a>
         <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarCollapse"
         >
            <span className="navbar-toggler-icon" />
         </button>
         <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav mr-auto"></ul>
            <ul className="nav navbar-nav navbar-right">
               <li className="nav-item active">
                  <a
                     className="nav-link"
                     href="/login"
                     style={{ fontWeight: 500 }}
                  >
                     Iniciar Sesión
                  </a>
               </li>
            </ul>
         </div>
      </nav>
   );
};

export default NavBar;
