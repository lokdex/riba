import { useState, useEffect, useCallback, useRef } from "react";
import axios from "axios";

const useFetch = () => {
   let [isLoading, setIsLoading] = useState(false);
   let isMounted = useRef();

   let fetch = useCallback(async ({ url, params = {}, success, failure }) => {
      let error = null;
      try {
         setIsLoading(true);
         let response = await axios({
            method: "POST",
            data: { paData: params, query: url },
         });
         if (isMounted.current) success(response.data);
      } catch (err) {
         if (err.response) {
            error = err.response.data;
         } else if (err.request) {
            error = "Error de red";
         } else {
            error = "Error desconocido";
         }
         failure(error);
      } finally {
         setIsLoading(false);
      }
   }, []);

   useEffect(() => {
      isMounted.current = true;
      return () => {
         isMounted.current = false;
      };
   }, []);

   return [fetch, isLoading];
};

export default useFetch;
