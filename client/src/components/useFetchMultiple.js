import { useState, useEffect, useCallback, useRef } from "react";
import axios from "axios";

const useFetchMultiple = () => {
   let [isLoading, setIsLoading] = useState(false);
   let isMounted = useRef();

   let fetch = useCallback(async ({ requests, success, failure }) => {
      let error = null;
      console.log(
         requests.map(({ url, params = {} }) => {
            console.log({ url, params });
            return axios({
               method: "POST",
               data: { paData: params, query: url },
            });
         })
      );
      try {
         setIsLoading(true);
         let responses = await Promise.all(
            requests.map(({ url, params = {} }) =>
               axios({
                  method: "POST",
                  data: { paData: params, query: url },
               })
            )
         );
         if (isMounted.current) success(responses);
      } catch (err) {
         if (err.response) {
            error = err.response.data;
         } else if (err.request) {
            error = "Error de red";
         } else {
            error = "Error desconocido";
         }
         failure(error);
      } finally {
         setIsLoading(false);
      }
   }, []);

   useEffect(() => {
      isMounted.current = true;
      return () => {
         isMounted.current = false;
      };
   }, []);

   return [fetch, isLoading];
};

export default useFetchMultiple;
