import React, { useEffect } from "react";
import { Router } from "@reach/router";
import "./App.css";

import NavBar from "./components/NavBar";

import Fpg1000 from "./views/Fpg1000"; // Dashboard
import Fpg1001 from "./views/Fpg1001"; // Dashboard
import Fpg1110 from "./views/Fpg1100/Fpg1110"; // Folders Index
import Fpg1120 from "./views/Fpg1100/Fpg1120"; // Folders Form
import Fpg1121 from "./views/Fpg1100/Fpg1121"; // Participantes Index
import Fpg1122 from "./views/Fpg1100/Fpg1122"; // Participantes Form
import Fpg1123 from "./views/Fpg1100/Fpg1123"; // Bib. Index
import Fpg1124 from "./views/Fpg1100/Fpg1124"; // Bib. Form
import Fpg1210 from "./views/Fpg1200/Fpg1210"; // Cursos Index
import Fpg1220 from "./views/Fpg1200/Fpg1220"; // Cursos Docs

import Test from "./views/Test";

import axios from "./axios";
/*
import FoldersForm from "./views/folders/FoldersForm";
import UpdateFolder from "./views/folders/UpdateFolder";
import Login from "./views/Login";
import Upload from "./views/Upload";
import Participantes from "./views/participantes/Participantes";
import ParticipanteForm from "./views/participantes/ParticipanteForm";
import Bibliografia from "./views/bibliografia/Bibliografia";
import BibliografiaForm from "./views/bibliografia/BibliografiaForm";
//import Eventos from "./views/Eventos";

// Import para cursos
import Cursos from "./views/cursos/Cursos";
import CursoDocs from "./views/cursos/CursoDocs";
*/

const App = () => {
   useEffect(() => {
      sessionStorage.setItem("CUSUCOD", "1221");
      async function getInit() {
         try {
            let response = await axios.post("/FPG1610I", {
               CUSUCOD: "1221",
               //CUNIACA: "40",
            });
            if (response?.data) {
               sessionStorage.setItem("OINIT", JSON.stringify(response.data));
            }
         } catch (error) {
            alert("Error al cargar datos");
         }
      }
      if (sessionStorage.getItem("CUSUCOD")) {
         getInit();
      }
   }, []);

   return (
      <Router>
         <Base path="/">
            <Fpg1000 path="/" default />
            <Fpg1001 path="Fpg1001" />
            <Fpg1110 path="Fpg1110" />
            <Fpg1120 path="Fpg1120" />
            <Fpg1121 path="Fpg1121" />
            <Fpg1122 path="Fpg1122" />
            <Fpg1123 path="Fpg1123" />
            <Fpg1124 path="Fpg1124" />
            <Fpg1210 path="Fpg1210" />
            <Fpg1220 path="Fpg1220" />
            <Test path="test" />
         </Base>
      </Router>
   );
};

const Base = ({ children }) => {
   return (
      <>
         <NavBar />
         {children}
      </>
   );
};
export default App;
